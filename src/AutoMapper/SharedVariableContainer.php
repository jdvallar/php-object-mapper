<?php


namespace Vallarj\ObjectMapper\AutoMapper;


class SharedVariableContainer
{
    /** @var array */
    private $variables = [];

    /**
     * Set a context variable
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function setVariable(string $name, $value): void
    {
        $this->variables[$name] = $value;
    }

    /**
     * Returns true if variable exists
     *
     * @param string $name
     * @return bool
     */
    public function hasVariable(string $name): bool
    {
        return isset($this->variables[$name]);
    }

    /**
     * Get a context variable
     *
     * @param string $name
     * @return mixed|null
     */
    public function getVariable(string $name)
    {
        return $this->variables[$name] ?? null;
    }
}
