<?php


namespace Vallarj\ObjectMapper\AutoMapper;


class Context
{
    /** @var array|null */
    private $selectedProperties;

    /** @var int */
    private $maxDepth;

    /** @var int */
    private $currentDepth;

    /** @var bool */
    private $targetResolved;

    /** @var array */
    private $variables;

    /** @var SharedVariableContainer */
    private $sharedVariables;

    /**
     * Context constructor.
     *
     * @param array|null $selectedProperties
     * @param int $maxDepth
     * @param int $currentDepth
     * @param bool $targetResolved
     * @param SharedVariableContainer $sharedVariables
     */
    public function __construct(
        ?array $selectedProperties,
        int $maxDepth,
        int $currentDepth,
        bool $targetResolved,
        SharedVariableContainer $sharedVariables
    ) {
        $this->selectedProperties = $selectedProperties;
        $this->maxDepth = $maxDepth;
        $this->currentDepth = $currentDepth;
        $this->targetResolved = $targetResolved;
        $this->variables = [];
        $this->sharedVariables = $sharedVariables;
    }

    /**
     * Returns true if a property name is selected
     *
     * @param string $propertyName
     * @return bool
     */
    public function isSelectedProperty(string $propertyName): bool
    {
        return is_null($this->selectedProperties)
            || in_array('*', $this->selectedProperties)
            || in_array($propertyName, $this->selectedProperties);
    }

    /**
     * Returns the context's selected properties
     *
     * @return array|null
     */
    public function getSelectedProperties(): ?array
    {
        return $this->selectedProperties;
    }

    /**
     * Returns the current depth of this context
     *
     * @return int
     */
    public function getCurrentDepth(): int
    {
        return $this->currentDepth;
    }

    /**
     * Returns the max depth of this context
     *
     * @return int
     */
    public function getMaxDepth(): int
    {
        return $this->maxDepth;
    }

    /**
     * Returns true if the current depth is at the max depth
     *
     * @return int
     */
    public function hasReachedMaxDepth(): int
    {
        return $this->currentDepth > $this->maxDepth;
    }

    /**
     * Returns true if the target was resolved
     *
     * @return bool
     */
    public function isTargetResolved(): bool
    {
        return $this->targetResolved;
    }

    /**
     * Set a context variable
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function setVariable(string $name, $value): void
    {
        $this->variables[$name] = $value;
    }

    /**
     * Returns true if variable exists
     *
     * @param string $name
     * @return bool
     */
    public function hasVariable(string $name): bool
    {
        return isset($this->variables[$name]);
    }

    /**
     * Get a context variable
     *
     * @param string $name
     * @return mixed|null
     */
    public function getVariable(string $name)
    {
        return $this->variables[$name] ?? null;
    }

    /**
     * Set a shared context variable. Shared variables are persisted across nested map calls
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function setSharedVariable(string $name, $value): void
    {
        $this->sharedVariables->setVariable($name, $value);
    }

    /**
     * Returns true if a shared context variable exists
     *
     * @param string $name
     * @return bool
     */
    public function hasSharedVariable(string $name): bool
    {
        return $this->sharedVariables->hasVariable($name);
    }

    /**
     * Get a shared context variable. Shared variables are persisted across nested map calls
     *
     * @param string $name
     * @return mixed|null
     */
    public function getSharedVariable(string $name)
    {
        return $this->sharedVariables->getVariable($name);
    }

    /**
     * Returns the shared variable container
     *
     * @return SharedVariableContainer
     */
    public function getSharedVariables(): SharedVariableContainer
    {
        return $this->sharedVariables;
    }
}
