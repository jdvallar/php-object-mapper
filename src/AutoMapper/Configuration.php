<?php


namespace Vallarj\ObjectMapper\AutoMapper;


class Configuration
{
    const MAX_DEPTH = "max_depth";

    /** @var int */
    private $maxDepth = 3;

    /**
     * Configuration constructor.
     *
     * @param array $options
     */
    public function __construct(array $options)
    {
        if (isset($options[self::MAX_DEPTH])) {
            $this->setMaxDepth($options[self::MAX_DEPTH]);
        }
    }

    /**
     * Returns the max depth.
     *
     * @return int
     */
    public function getMaxDepth(): int
    {
        return $this->maxDepth;
    }

    /**
     * Sets the max depth
     *
     * @param int $maxDepth
     * @return void
     */
    private function setMaxDepth(int $maxDepth): void
    {
        $this->maxDepth = $maxDepth;
    }
}
