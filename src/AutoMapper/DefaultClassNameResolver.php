<?php


namespace Vallarj\ObjectMapper\AutoMapper;


use Doctrine\ORM\Proxy\Proxy;
use ProxyManager\Proxy\GhostObjectInterface;

class DefaultClassNameResolver implements ClassNameResolver
{
    /**
     * @inheritDoc
     */
    public function getClass($object): string
    {
        if ($object instanceof Proxy || $object instanceof GhostObjectInterface) {
            return get_parent_class($object);
        }

        return get_class($object);
    }
}
