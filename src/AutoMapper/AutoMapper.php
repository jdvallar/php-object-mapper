<?php


namespace Vallarj\ObjectMapper\AutoMapper;


use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Exception\MapperAlreadyRegisteredException;
use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Exception\MaxDepthReachedException;
use Vallarj\ObjectMapper\Mapper\MapperInterface;

class AutoMapper implements AutoMapperInterface
{
    /** @var MapperInterface[] */
    private $mappers = [];

    /** @var Configuration */
    private $configuration;

    /** @var ClassNameResolver */
    private $classNameResolver;

    /**
     * AutoMapper constructor.
     *
     * @param Configuration $configuration
     * @param ClassNameResolver|null $classNameResolver
     */
    public function __construct(Configuration $configuration, ?ClassNameResolver $classNameResolver = null)
    {
        $this->configuration = $configuration;

        if (is_null($classNameResolver)) {
            $classNameResolver = new DefaultClassNameResolver();
        }

        $this->classNameResolver = $classNameResolver;
    }

    /**
     * @inheritDoc
     * @throws MaxDepthReachedException
     */
    final public function map($source, $target, ?array $properties = null)
    {
        return $this->autoMap($source, $target, $properties);
    }

    /**
     * @inheritDoc
     * @throws MaxDepthReachedException
     */
    final public function mapWithContext($source, $target, Context $context, string $propertyName)
    {
        return $this->autoMap($source, $target, null, $context, $propertyName);
    }

    /**
     * Auto-maps a source to a target.
     *
     * @param mixed $source
     * @param mixed $target
     * @param array|null $properties
     * @param Context|null $context
     * @param string|null $propertyName
     * @return mixed
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     * @throws InvalidArgumentException
     */
    private function autoMap(
        $source,
        $target,
        ?array $properties = null,
        ?Context $context = null,
        ?string $propertyName = null
    ) {
        // If source is null, return null
        if (is_null($source)) {
            return null;
        }

        $sourceClass = $this->classNameResolver->getClass($source);

        if (is_array($target)) {
            $targetClass = null;
            foreach ($target as $testClass) {
                if (!is_string($testClass)) {
                    throw new InvalidArgumentException(
                        "Only class names are allowed for multiple target mapping"
                    );
                }

                if ($this->hasMapper($sourceClass, $testClass)) {
                    $targetClass = $testClass;
                    break;
                }
            }
            if (!$targetClass) {
                throw new MapperNotRegisteredException("Mapping for [$sourceClass, {" .
                    implode(', ', $target) . "}] not found.");
            }
            $targetIsClassName = true;
        } else if (is_string($target) && class_exists($target)) {
            $targetIsClassName = true;
            $targetClass = $target;
        } else {
            $targetIsClassName = false;
            $targetClass = $this->classNameResolver->getClass($target);
        }

        // Get mapper
        $mapper = $this->getMapper($sourceClass, $targetClass);

        // Create context
        if (!$context) {
            // Create new context
            $context = new Context(
                $properties,
                $this->configuration->getMaxDepth(),
                1,
                $targetIsClassName,
                new SharedVariableContainer()
            );
        } else {
            // Get next context properties
            $properties = [];
            $contextProperties = $context->getSelectedProperties();
            if (!is_null($contextProperties)) {
                $propertyFound = false;
                foreach ($contextProperties as $property) {
                    if ($property == '*' || $propertyName == $property) {
                        $propertyFound = true;
                    }

                    // explode property by '.'
                    $levels = explode('.', $property);
                    if (count($levels) > 1 && $levels[0] == $propertyName) {
                        $properties[] = implode('.', array_slice($levels, 1));
                    }
                }

                // if property not found, return null;
                if (!$propertyFound) {
                    return null;
                }
            } else {
                $properties = null;
            }

            // Update context. Assume next depth
            $context = new Context(
                $properties,
                $context->getMaxDepth(),
                $context->getCurrentDepth() + 1,
                $targetIsClassName,
                $context->getSharedVariables()
            );
        }

        if ($context->hasReachedMaxDepth()) {
            throw new MaxDepthReachedException("Max depth has been reached by mapper.");
        }

        if ($targetIsClassName) {
            $target = $mapper->resolveTargetObject($this, $source, $targetClass, $context);
        }

        return $mapper->map($this, $source, $target, $context);
    }

    /**
     * Registers a mapper.
     *
     * @param MapperInterface $mapper
     * @return void
     * @throws MapperAlreadyRegisteredException
     * @throws MapperNotRegisteredException
     */
    final public function register(MapperInterface $mapper): void
    {
        $source = $mapper->getSourceClass();
        $target = $mapper->getTargetClass();

        if ($this->hasMapper($source, $target)) {
            $duplicate = get_class($this->getMapper($source, $target));
            throw new MapperAlreadyRegisteredException("Unable to register " . get_class($mapper) .
                " for mapping [$source, $target]. (Duplicate of $duplicate)");
        }

        $this->addMapper($mapper);
    }

    /**
     * Returns true if mapper exists.
     *
     * @param string $source
     * @param string $target
     * @return bool
     */
    private function hasMapper(string $source, string $target): bool
    {
        return isset($this->mappers[$source][$target]);
    }

    /**
     * Gets a registered mapper.
     *
     * @param string $source
     * @param string $target
     * @throws MapperNotRegisteredException
     * @return MapperInterface
     */
    private function getMapper(string $source, string $target): MapperInterface
    {
        if (!$this->hasMapper($source, $target)) {
            throw new MapperNotRegisteredException("Mapping for [$source, $target] not found.");
        }

        return $this->mappers[$source][$target];
    }

    /**
     * Adds a mapper.
     *
     * @param MapperInterface $mapper
     * @return void
     */
    private function addMapper(MapperInterface $mapper): void
    {
        $this->mappers[$mapper->getSourceClass()][$mapper->getTargetClass()] = $mapper;
    }
}
