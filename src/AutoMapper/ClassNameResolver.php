<?php


namespace Vallarj\ObjectMapper\AutoMapper;


interface ClassNameResolver
{
    /**
     * Returns the class name of a given object
     *
     * @param mixed $object
     * @return string
     */
    public function getClass($object): string;
}
