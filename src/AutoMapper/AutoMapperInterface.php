<?php


namespace Vallarj\ObjectMapper\AutoMapper;


use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Exception\MaxDepthReachedException;

interface AutoMapperInterface
{
    /**
     * Maps a source object to a target object
     * Returns the target object
     *
     * @param mixed $source
     * @param mixed $target
     * @param array|null $properties
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     * @return mixed
     */
    public function map($source, $target, ?array $properties = null);

    /**
     * Maps a source object to a target object assuming a provided context
     *
     * @param mixed $source
     * @param mixed $target
     * @param Context $context
     * @param string $propertyName
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     * @return mixed
     */
    public function mapWithContext($source, $target, Context $context, string $propertyName);
}
