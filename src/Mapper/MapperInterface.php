<?php


namespace Vallarj\ObjectMapper\Mapper;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;

interface MapperInterface
{
    /**
     * Returns the source class
     *
     * @return string
     */
    public function getSourceClass(): string;

    /**
     * Returns the target class
     *
     * @return string
     */
    public function getTargetClass(): string;

    /**
     * Maps a source object to a target object.
     *
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param mixed $target
     * @param Context $context
     * @return mixed
     */
    public function map(AutoMapperInterface $autoMapper, $source, $target, Context $context);

    /**
     * Resolves a target object.
     *
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param string $targetClass
     * @param Context $context
     * @return mixed
     */
    public function resolveTargetObject(
        AutoMapperInterface $autoMapper,
        $source,
        string $targetClass,
        Context $context
    );
}
