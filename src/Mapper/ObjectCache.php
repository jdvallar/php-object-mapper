<?php


namespace Vallarj\ObjectMapper\Mapper;


use Vallarj\ObjectMapper\Exception\ItemNotCachedException;

class ObjectCache
{
    /** @var CacheItem[][] */
    private $objectCache = [];

    /**
     * Adds an object to the cache
     *
     * @param string $class
     * @param string $identifier
     * @param mixed $object
     * @return void
     */
    public function addObject(string $class, string $identifier, $object): void
    {
        $this->objectCache[$class][$identifier] = new CacheItem($object);
    }

    /**
     * Returns true if object exists in the cache
     *
     * @param string $class
     * @param string $identifier
     * @return bool
     */
    public function hasObject(string $class, string $identifier): bool
    {
        return isset($this->objectCache[$class][$identifier]);
    }

    /**
     * Returns the cached object if it exists
     *
     * @param string $class
     * @param string $identifier
     * @throws ItemNotCachedException
     * @return mixed
     */
    public function getObject(string $class, string $identifier)
    {
        if (!$this->hasObject($class, $identifier)) {
            throw new ItemNotCachedException("Item [$class:$identifier] not found in cache.");
        }

        return $this->objectCache[$class][$identifier]->getObject();
    }

    /**
     * Returns the cached properties of a given class and identifier
     *
     * @param string $class
     * @param string $identifier
     * @return array
     * @throws ItemNotCachedException
     */
    public function getCachedProperties(string $class, string $identifier): array
    {
        if (!$this->hasObject($class, $identifier)) {
            throw new ItemNotCachedException("Item [$class:$identifier] not found in cache.");
        }

        return $this->objectCache[$class][$identifier]->getProperties();
    }

    /**
     * Returns true if the specified cached object property has been cached
     *
     * @param string $property
     * @param string $class
     * @param string $identifier
     * @param int $contextDepth
     * @return bool
     */
    public function hasCachedProperty(string $property, string $class, string $identifier, int $contextDepth): bool
    {
        if (!$this->hasObject($class, $identifier)) {
            return false;
        }

        return $this->objectCache[$class][$identifier]->hasCachedProperty($property, $contextDepth);
    }

    /**
     * Returns true if the specified cached object property has been cached in a given context level
     *
     * @param string $property
     * @param string $class
     * @param string $identifier
     * @param int $contextDepth
     * @return bool
     */
    public function hasCachedPropertyDepth(
        string $property,
        string $class,
        string $identifier,
        int $contextDepth
    ): bool {
        if (!$this->hasObject($class, $identifier)) {
            return false;
        }

        return $this->objectCache[$class][$identifier]->hasCachedPropertyDepth($property, $contextDepth);
    }

    /**
     * Updates a flagged cached property of a cached object
     *
     * @param string $class
     * @param string $identifier
     * @param string $property
     * @param int $contextDepth
     * @param bool $complete
     * @return void
     */
    public function updateCachedProperty(
        string $class,
        string $identifier,
        string $property,
        int $contextDepth,
        bool $complete
    ): void {
        if (!$this->hasObject($class, $identifier)) {
            return;
        }

        $this->objectCache[$class][$identifier]->setCachedProperty($property, $contextDepth, $complete);
    }
}
