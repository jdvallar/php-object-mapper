<?php


namespace Vallarj\ObjectMapper\Mapper;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;

interface Map
{
    /**
     * Returns the property name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Returns true if property should be mapped on target resolution
     *
     * @return bool
     */
    public function shouldMapOnCreate(): bool;

    /**
     * Maps a source property to a target property
     *
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param mixed $target
     * @param Context $context
     * @return void
     */
    public function map(AutoMapperInterface $autoMapper, $source, $target, Context $context): void;
}
