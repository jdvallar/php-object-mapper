<?php


namespace Vallarj\ObjectMapper\Mapper;


class CacheItem
{
    /** @var mixed */
    private $object;

    /** @var array[] */
    private $properties;

    /**
     * CacheItem constructor.
     *
     * @param mixed $object
     */
    public function __construct($object)
    {
        $this->object = $object;
        $this->properties = [];
    }

    /**
     * Returns the cached object
     *
     * @return mixed
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Returns the properties updated in the cached item
     *
     * @return array
     */
    public function getProperties(): array
    {
        return array_keys($this->properties);
    }

    /**
     * Returns true if item has the specified property cached at the specified depth
     *
     * @param string $property
     * @param int $contextDepth
     * @return bool
     */
    public function hasCachedPropertyDepth(string $property, int $contextDepth): bool
    {
        return array_key_exists($property, $this->properties)
            && $this->properties[$property]['depth'] <= $contextDepth;
    }

    /**
     * Returns true if item has a completely cached property
     *
     * @param string $property
     * @param int $contextDepth
     * @return bool
     */
    public function hasCachedProperty(string $property, int $contextDepth): bool
    {
        return array_key_exists($property, $this->properties)
            && (
                $this->properties[$property]['complete']
                || $this->properties[$property]['depth'] <= $contextDepth
            );
    }

    /**
     * Sets the property as cached
     *
     * @param string $property
     * @param int $contextDepth
     * @param bool $complete
     * @return void
     */
    public function setCachedProperty(string $property, int $contextDepth, bool $complete): void
    {
        if (
            !array_key_exists($property, $this->properties)
            || ($complete && $contextDepth < $this->properties[$property]['depth'])
            || ($contextDepth < $this->properties[$property]['depth'] && !$this->properties[$property]['complete'])
        ) {
            $this->properties[$property] = [
                'depth' => $contextDepth,
                'complete' => $complete
            ];
        }
    }

    /**
     * Updates the current updated properties
     *
     * @param array $updatedProperties
     * @return void
     */
    public function updateProperties(array $updatedProperties): void
    {
        foreach ($updatedProperties as $updatedProperty) {
            $this->properties[$updatedProperty] = 1;
        }
    }
}
