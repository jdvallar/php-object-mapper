<?php


namespace Vallarj\ObjectMapper\Mapper\Map;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Exception\MaxDepthReachedException;
use Vallarj\ObjectMapper\Mapper\Map;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;

class ToOneAssociationMap implements Map
{
    /** @var string */
    private $name;

    /** @var string[] */
    private $targetClasses;

    /** @var bool */
    private $mapOnCreate;

    /** @var MappingStrategy */
    private $mappingStrategy;

    /**
     * ToOneAssociationMap constructor.
     *
     * @param string $name
     * @param array $targetClasses
     * @param bool $mapOnCreate
     * @param MappingStrategy $mappingStrategy
     */
    public function __construct(
        string $name,
        array $targetClasses,
        bool $mapOnCreate,
        MappingStrategy $mappingStrategy
    ) {
        $this->name = $name;
        $this->targetClasses = $targetClasses;
        $this->mapOnCreate = $mapOnCreate;
        $this->mappingStrategy = $mappingStrategy;
    }

    /**
     * Returns the property name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Returns the target class
     *
     * @return string[]
     */
    public function getTargetClasses(): array
    {
        return $this->targetClasses;
    }

    /**
     * @inheritDoc
     */
    public function shouldMapOnCreate(): bool
    {
        return $this->mapOnCreate;
    }

    /**
     * Returns the property value of a given source object
     *
     * @param mixed $source
     * @return mixed
     */
    public function getSourceValue($source)
    {
        return $this->mappingStrategy->getValue($this->name, $source);
    }

    /**
     * Sets the property value of a given target object
     *
     * @param mixed $target
     * @param mixed $value
     * @return void
     */
    public function setTargetValue($target, $value): void
    {
        $this->mappingStrategy->setValue($this->name, $target, $value);
    }

    /**
     * Maps a source property to a target property
     *
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param mixed $target
     * @param Context $context
     * @return void
     * @throws MapperNotRegisteredException
     */
    public function map(AutoMapperInterface $autoMapper, $source, $target, Context $context): void
    {
        $this->setTargetValue(
            $target,
            $autoMapper->mapWithContext(
                $this->getSourceValue($source),
                $this->getTargetClasses(),
                $context,
                $this->name
            )
        );
    }
}
