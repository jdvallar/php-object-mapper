<?php


namespace Vallarj\ObjectMapper\Mapper\Map;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Mapper\Map;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;

class AttributeMap implements Map
{
    /** @var string */
    private $name;

    /** @var bool */
    private $mapOnCreate;

    /** @var MappingStrategy */
    private $mappingStrategy;

    /**
     * AttributeMap constructor.
     *
     * @param string $name
     * @param bool $mapOnCreate
     * @param MappingStrategy $mappingStrategy
     */
    public function __construct(string $name, bool $mapOnCreate, MappingStrategy $mappingStrategy)
    {
        $this->name = $name;
        $this->mapOnCreate = $mapOnCreate;
        $this->mappingStrategy = $mappingStrategy;
    }

    /**
     * Returns the property name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function shouldMapOnCreate(): bool
    {
        return $this->mapOnCreate;
    }

    /**
     * Returns the property value of a given source object
     *
     * @param mixed $source
     * @return mixed
     */
    public function getSourceValue($source)
    {
        return $this->mappingStrategy->getValue($this->name, $source);
    }

    /**
     * Sets the property value of a given target object
     *
     * @param mixed $target
     * @param mixed $value
     * @return void
     */
    public function setTargetValue($target, $value): void
    {
        $this->mappingStrategy->setValue($this->name, $target, $value);
    }

    /**
     * @inheritDoc
     */
    public function map(AutoMapperInterface $autoMapper, $source, $target, Context $context): void
    {
        $this->setTargetValue($target, $this->getSourceValue($source));
    }
}
