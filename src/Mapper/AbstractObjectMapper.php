<?php


namespace Vallarj\ObjectMapper\Mapper;


use ArgumentCountError;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Exception\CreateTargetObjectException;
use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Exception\InvalidResolvedTargetObjectException;
use Vallarj\ObjectMapper\Exception\ItemNotCachedException;
use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Mapper\Map\AttributeMap;
use Vallarj\ObjectMapper\Mapper\Map\ToManyAssociationMap;
use Vallarj\ObjectMapper\Mapper\Map\ToOneAssociationMap;
use Vallarj\ObjectMapper\Mapper\Strategy\GetterAdderMappingStrategy;
use Vallarj\ObjectMapper\Mapper\Strategy\GetterSetterMappingStrategy;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;
use Vallarj\ObjectMapper\Mapper\Strategy\NullMappingStrategy;

abstract class AbstractObjectMapper implements MapperInterface
{
    private const CTX_RESOLVED_ASSOCIATION = "__AOM__RESOLVED_ASSOCIATION";
    private const CTX_RESOLVED_CACHE = "__AOM__RESOLVED_CACHE";
    private const CTX_RESOLVED_TARGET = "__AOM__RESOLVED_TARGET";
    private const CTX_CREATED_TARGET = "__AOM__CREATED_TARGET";
    private const CTX_OBJECT_CACHE = "__AOM__OBJECT_CACHE";

    /** @var string */
    protected $sourceClass;

    /** @var string */
    protected $targetClass;

    /** @var AttributeMap */
    private $identifier;

    /** @var AttributeMap[] */
    private $attributeMaps = [];

    /** @var ToOneAssociationMap[] */
    private $toOneAssociationMaps = [];

    /** @var ToManyAssociationMap[] */
    private $toManyAssociationMaps = [];

    /**
     * @inheritDoc
     */
    public function getSourceClass(): string
    {
        return $this->sourceClass;
    }

    /**
     * @inheritDoc
     */
    public function getTargetClass(): string
    {
        return $this->targetClass;
    }

    /**
     * Creates a target object. This is called when target supplied to automapper is a class name
     * instead of an object.
     *
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param string $targetClass
     * @param Context $context
     * @return mixed
     * @throws CreateTargetObjectException
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        try {
            return new $targetClass();
        } catch (ArgumentCountError $e) {
            throw new CreateTargetObjectException("Unable to create target object without parameters." .
                " Override " . get_class($this) . "::createTargetObject to customize object creation.");
        }
    }

    /**
     * Resolves a target on association.
     * Return null to skip this method.
     *
     * @param mixed $source
     * @param string $targetClass
     * @param Context $context
     * @return mixed|null
     */
    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        return null;
    }

    /**
     * Returns the null mapping strategy
     *
     * @return MappingStrategy
     */
    public function getNullMappingStrategy(): MappingStrategy
    {
        return new NullMappingStrategy();
    }

    /**
     * Returns the default one-to-one mapping strategy
     *
     * @return MappingStrategy
     */
    public function getDefaultOneToOneMappingStrategy(): MappingStrategy
    {
        return new GetterSetterMappingStrategy();
    }

    /**
     * Returns the default one-to-many mapping strategy
     *
     * @return MappingStrategy
     */
    public function getDefaultOneToManyMappingStrategy(): MappingStrategy
    {
        return new GetterAdderMappingStrategy();
    }

    /**
     * @inheritDoc
     * @throws MapperNotRegisteredException
     */
    final public function map(AutoMapperInterface $autoMapper, $source, $target, Context $context)
    {
        // Make sure the cache is added to the context on the very first call
        if ($context->getCurrentDepth() == 1) {
            $this->getObjectCache($context);
        }

        // If target is not resolved from cache
        // and is a direct target provided by AutoMapper or
        // is resolved from object mapper and identifier should be mapped upon creation
        if (!$context->getVariable(self::CTX_RESOLVED_CACHE)
            && (
                !$context->hasVariable(self::CTX_RESOLVED_TARGET)
                || $this->getIdentifier()->shouldMapOnCreate()
            )
        ) {
            // Cache the target object
            $identifier = $this->getSourceIdentifier($source);
            if ($identifier) {
                $this->addToObjectCache($identifier, $target, $context);
            }
        }

        $selectedProperties = $context->getSelectedProperties();
        $nullProps = is_null($selectedProperties);

        // If selectedProperties is null, it means all properties are included
        if (is_null($selectedProperties) || in_array('*', $selectedProperties)) {
            $selectedProperties = array_unique(array_merge(
                $selectedProperties ?? [],
                [$this->identifier->getName()],
                array_keys($this->attributeMaps),
                array_keys($this->toOneAssociationMaps),
                array_keys($this->toManyAssociationMaps)
            ));
        }

        if ($context->getVariable(self::CTX_RESOLVED_CACHE)) {
            $identifier = $this->getSourceIdentifier($source);

            // If all properties in this context are already cached
            if ($this->hasCachedObjectProperty($identifier, $selectedProperties, $nullProps, $context)) {
                return $target;
            }
        }

        // If resolved target object was not from an external source
        // (i.e., either object was resolved from cache or was created)
        if (!$context->getVariable(self::CTX_RESOLVED_ASSOCIATION)) {
            if (
                $context->getVariable(self::CTX_CREATED_TARGET)
                && !$this->getIdentifier()->shouldMapOnCreate()
            ) {
                $identifier = "";
            } else {
                $identifier = $this->getSourceIdentifier($source);
            }

            // Get all top level properties
            $topLevelProperties = [];
            foreach ($selectedProperties as $property) {
                $exploded = explode('.', $property);
                $topLevelProperties[$exploded[0]][] = $property;
            }

            // Map identifier
            $identifierMap = $this->getIdentifier();
            if (
                !($this->hasCachedObjectProperty($identifier, $identifierMap->getName(), $nullProps, $context))
                && ($context->getCurrentDepth() > 1 || $this->shouldMapProperty($identifierMap, $context))
            ) {
                $identifierMap->map($autoMapper, $source, $target, $context);
                $this->updateCachedObjectProperty($identifier, $identifierMap->getName(), true, $context);
            }

            if ($context->getCurrentDepth() < $context->getMaxDepth()) {
                // Map attributes
                foreach ($this->attributeMaps as $attributeMap) {
                    if (
                        $this->shouldMapProperty($attributeMap, $context)
                        && !$this->hasCachedObjectProperty($identifier, $attributeMap->getName(), $nullProps, $context)
                    ) {
                        $attributeMap->map($autoMapper, $source, $target, $context);
                        $this->updateCachedObjectProperty($identifier, $attributeMap->getName(), true, $context);
                    }
                }

                // Map to-one associations
                $associationMaps = array_merge($this->toOneAssociationMaps, $this->toManyAssociationMaps);
                foreach ($associationMaps as $associationMap) {
                    $assocProps = $topLevelProperties[$associationMap->getName()] ?? [];
                    if (
                        $this->shouldMapProperty($associationMap, $context)
                        && !$this->hasCachedObjectProperty(
                            $identifier,
                            $assocProps,
                            $nullProps,
                            $context
                        )
                    ) {
                        $associationMap->map($autoMapper, $source, $target, $context);

                        if ($nullProps) {
                            foreach ($assocProps as $prop) {
                                $this->updateCachedObjectProperty($identifier, $prop, true, $context);
                            }
                        } else {
                            foreach ($assocProps as $prop) {
                                $assocCount = substr_count('.', $prop) + 1;
                                $depthDiff = $context->getMaxDepth() - $context->getCurrentDepth();
                                $this->updateCachedObjectProperty(
                                    $identifier,
                                    $prop,
                                    $assocCount < $depthDiff,
                                    $context
                                );
                            }
                        }
                    }
                }
            }
        }

        return $target;
    }

    /**
     * Sets the identifier name
     *
     * @param null|string $identifierName
     * @param bool $mapOnCreate
     * @param MappingStrategy|null $mappingStrategy
     * @return static
     */
    final public function setIdentifier(
        ?string $identifierName,
        bool $mapOnCreate = true,
        ?MappingStrategy $mappingStrategy = null
    ): AbstractObjectMapper {
        if (is_null($identifierName)) {
            $identifierName = '';
            $mappingStrategy = $this->getNullMappingStrategy();
        } else {
            $mappingStrategy = $mappingStrategy ?? $this->getDefaultOneToOneMappingStrategy();
        }

        $this->identifier = new AttributeMap($identifierName, $mapOnCreate, $mappingStrategy);
        return $this;
    }

    /**
     * Creates an attribute mapping with a given mapping strategy.
     * If mapping strategy is not defined, defaults to getter/setter mapping strategy
     *
     * @param string $propertyName
     * @param bool $mapOnCreate
     * @param MappingStrategy|null $mappingStrategy
     * @return static
     */
    final public function mapAttribute(
        string $propertyName,
        bool $mapOnCreate = true,
        ?MappingStrategy $mappingStrategy = null
    ): AbstractObjectMapper {
        $mappingStrategy = $mappingStrategy ?? $this->getDefaultOneToOneMappingStrategy();
        $this->attributeMaps[$propertyName] = new AttributeMap($propertyName, $mapOnCreate, $mappingStrategy);
        return $this;
    }

    /**
     * Creates a to-one association mapping with a given mapping strategy.
     * If mapping strategy is not defined, defaults to getter/setter mapping strategy
     *
     * @param string $propertyName
     * @param string|array $target
     * @param bool $mapOnCreate
     * @param MappingStrategy|null $mappingStrategy
     * @return static
     * @throws InvalidArgumentException
     */
    final public function mapToOneAssociation(
        string $propertyName,
        $target,
        bool $mapOnCreate = true,
        ?MappingStrategy $mappingStrategy = null
    ): AbstractObjectMapper {
        if (is_string($target)) {
            $target = [$target];
        }

        if (!is_array($target)) {
            throw new InvalidArgumentException("Target must be an FQCN or an array of FQCNs");
        }

        $mappingStrategy = $mappingStrategy ?? $this->getDefaultOneToOneMappingStrategy();
        $this->toOneAssociationMaps[$propertyName] = new ToOneAssociationMap(
            $propertyName,
            $target,
            $mapOnCreate,
            $mappingStrategy
        );
        return $this;
    }

    /**
     * Creates a to-many association mapping with a given mapping strategy.
     * If mapping strategy is not defined, defaults to getter/setter mapping strategy.
     *
     * @param string $propertyName
     * @param string|array $target
     * @param bool $mapOnCreate
     * @param MappingStrategy|null $mappingStrategy
     * @return static
     * @throws InvalidArgumentException
     */
    final public function mapToManyAssociation(
        string $propertyName,
        $target,
        bool $mapOnCreate = true,
        ?MappingStrategy $mappingStrategy = null
    ): AbstractObjectMapper {
        if (is_string($target)) {
            $target = [$target];
        }

        if (!is_array($target)) {
            throw new InvalidArgumentException("Target must be an FQCN or an array of FQCNs");
        }

        $mappingStrategy = $mappingStrategy ?? $this->getDefaultOneToManyMappingStrategy();
        $this->toManyAssociationMaps[$propertyName] = new ToManyAssociationMap(
            $propertyName,
            $target,
            $mapOnCreate,
            $mappingStrategy
        );
        return $this;
    }

    /**
     * Returns the source identifier name
     *
     * @param mixed $source
     * @return mixed
     */
    final public function getSourceIdentifier($source)
    {
        return $this->getIdentifier()->getSourceValue($source);
    }

    /**
     * @inheritDoc
     * @throws CreateTargetObjectException
     * @throws InvalidResolvedTargetObjectException
     * @throws ItemNotCachedException
     */
    final public function resolveTargetObject(
        AutoMapperInterface $autoMapper,
        $source,
        string $targetClass,
        Context $context
    ) {
        $context->setVariable(self::CTX_RESOLVED_TARGET, true);
        $identifier = $this->getIdentifier()->shouldMapOnCreate()
            ? $this->getSourceIdentifier($source)
            : null;

        if ($identifier && $this->isCachedObject($identifier, $context)) {
            $context->setVariable(self::CTX_RESOLVED_CACHE, true);
            return $this->getCachedObject($identifier, $context);
        }

        if ($context->getCurrentDepth() >= 2 &&
            !is_null($resolved = $this->resolveTargetOnAssociation($source, $targetClass, $context))) {
            $context->setVariable(self::CTX_RESOLVED_ASSOCIATION, true);

            if (!$resolved instanceof $targetClass) {
                throw new InvalidResolvedTargetObjectException("Resolved object must be of type "
                    . $targetClass);
            }

            return $resolved;
        }

        $context->setVariable(self::CTX_CREATED_TARGET, true);
        $object = $this->createTargetObject($autoMapper, $source, $targetClass, $context);
        if (!$object instanceof $targetClass) {
            throw new InvalidResolvedTargetObjectException("Resolved object must be of type " . $targetClass);
        }

        return $object;
    }

    /**
     * Returns the object cache from a specified context
     *
     * @param Context $context
     * @return ObjectCache
     */
    private function getObjectCache(Context $context): ObjectCache
    {
        if (!$context->hasSharedVariable(self::CTX_OBJECT_CACHE)) {
            $context->setSharedVariable(self::CTX_OBJECT_CACHE, new ObjectCache());
        }

        return $context->getSharedVariable(self::CTX_OBJECT_CACHE);
    }

    /**
     * Adds an object to the object cache
     *
     * @param string $identifier
     * @param mixed $object
     * @param Context $context
     * @return void
     */
    private function addToObjectCache(string $identifier, $object, Context $context): void
    {
        $cache = $this->getObjectCache($context);
        $cache->addObject(
            $this->targetClass,
            $identifier,
            $object
        );
    }

    /**
     * Returns true if object with specified identifier is in cache
     *
     * @param string $identifier
     * @param Context $context
     * @return bool
     */
    private function isCachedObject(string $identifier, Context $context): bool
    {
        $cache = $this->getObjectCache($context);
        return $cache->hasObject($this->targetClass, $identifier);
    }

    /**
     * Returns the cached object.
     *
     * @param string $identifier
     * @param Context $context
     * @return mixed
     * @throws ItemNotCachedException
     */
    private function getCachedObject(string $identifier, Context $context)
    {
        $cache = $this->getObjectCache($context);
        return $cache->getObject($this->targetClass, $identifier);
    }

    /**
     * Returns true if the cached object has already cached the specified property
     *
     * @param string $identifier
     * @param string|array $property
     * @param bool $nullProps
     * @param Context $context
     * @return bool
     */
    private function hasCachedObjectProperty(string $identifier, $property, bool $nullProps, Context $context): bool
    {
        $cache = $this->getObjectCache($context);
        if (is_string($property)) {
            return $nullProps ?
                $cache->hasCachedPropertyDepth(
                    $property,
                    $this->targetClass,
                    $identifier,
                    $context->getCurrentDepth()
                ) :
                $cache->hasCachedProperty($property, $this->targetClass, $identifier, $context->getCurrentDepth());
        } else if (is_array($property)) {
            if ($nullProps) {
                foreach ($property as $item) {
                    if (!$cache->hasCachedPropertyDepth(
                        $item,
                        $this->targetClass,
                        $identifier,
                        $context->getCurrentDepth()
                    )) {
                        return false;
                    }
                }
            } else {
                foreach ($property as $item) {
                    if (!$cache->hasCachedProperty(
                        $item,
                        $this->targetClass,
                        $identifier,
                        $context->getCurrentDepth()
                    )) {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates a cached object property
     *
     * @param string $identifier
     * @param string $property
     * @param bool $complete
     * @param Context $context
     * @return void
     */
    private function updateCachedObjectProperty(
        string $identifier,
        string $property,
        bool $complete,
        Context $context
    ): void {
        $cache = $this->getObjectCache($context);
        $cache->updateCachedProperty(
            $this->targetClass,
            $identifier,
            $property,
            $context->getCurrentDepth(),
            $complete
        );
    }

    /**
     * Returns the identifier map
     *
     * @return AttributeMap
     */
    private function getIdentifier(): AttributeMap
    {
        if (is_null($this->identifier)) {
            $this->identifier = new AttributeMap('id', true, $this->getDefaultOneToOneMappingStrategy());
        }

        return $this->identifier;
    }

    /**
     * Returns true if property should be mapped
     *
     * @param Map $map
     * @param Context $context
     * @return bool
     */
    private function shouldMapProperty(Map $map, Context $context): bool
    {
        return $context->isSelectedProperty($map->getName())
            && (
                !$context->hasVariable(self::CTX_RESOLVED_TARGET)
                || $map->shouldMapOnCreate()
            );
    }
}
