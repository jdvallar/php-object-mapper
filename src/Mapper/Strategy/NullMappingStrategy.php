<?php


namespace Vallarj\ObjectMapper\Mapper\Strategy;


class NullMappingStrategy implements MappingStrategy
{
    /**
     * @inheritDoc
     */
    public function getValue(string $propertyName, $source)
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $propertyName, $target, $value): void
    {
        return;
    }
}
