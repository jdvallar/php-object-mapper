<?php


namespace Vallarj\ObjectMapper\Mapper\Strategy;


use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\Inflector\Rules\Pattern;
use Doctrine\Inflector\Rules\Patterns;
use Doctrine\Inflector\Rules\Ruleset;
use Doctrine\Inflector\Rules\Substitutions;
use Doctrine\Inflector\Rules\Transformation;
use Doctrine\Inflector\Rules\Transformations;

class GetterAdderMappingStrategy implements MappingStrategy
{
    /** @var null|Inflector */
    private static $inflector = null;

    /**
     * Returns the default static inflector
     *
     * @return Inflector
     */
    public function getInflector(): Inflector
    {
        if (is_null(self::$inflector)) {
            $customSingularization = new Ruleset(
                new Transformations(new Transformation(new Pattern('/^(.*)sOf(.*)$/'), '\1Of\2')),
                new Patterns(),
                new Substitutions()
            );

            $factory = InflectorFactory::create();
            $factory->withSingularRules($customSingularization);
            self::$inflector =  $factory->build();
        }

        return self::$inflector;
    }

    /**
     * Sets the default static inflector
     *
     * @param Inflector $inflector
     * @return void
     */
    public function setInflector(Inflector $inflector): void
    {
        self::$inflector = $inflector;
    }

    /**
     * @inheritDoc
     */
    public function getValue(string $propertyName, $source)
    {
        return $source->{'get' . ucfirst($propertyName)}();
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $propertyName, $target, $value): void
    {
        $inflector = $this->getInflector();
        $singularMapping = $inflector->singularize($propertyName);

        // Check if item is already in the list
        $currentItems = $this->getValue($propertyName, $target);
        $currentItemHash = [];
        foreach ($currentItems as $currentItem) {
            $currentItemHash[spl_object_hash($currentItem)] = true;
        }

        foreach ($value as $item) {
            if (!isset($currentItemHash[spl_object_hash($item)])) {
                $target->{'add' . ucfirst($singularMapping)}($item);
            }
        }
    }
}
