<?php


namespace Vallarj\ObjectMapper\Mapper\Strategy;


interface MappingStrategy
{
    /**
     * Returns the property value of the source object
     *
     * @param string $propertyName
     * @param mixed $source
     * @return mixed
     */
    public function getValue(string $propertyName, $source);

    /**
     * Sets the value of a given target object property
     *
     * @param string $propertyName
     * @param mixed $target
     * @param mixed $value
     * @return void
     */
    public function setValue(string $propertyName, $target, $value): void;
}
