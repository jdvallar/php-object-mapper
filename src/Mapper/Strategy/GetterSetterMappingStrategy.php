<?php


namespace Vallarj\ObjectMapper\Mapper\Strategy;


class GetterSetterMappingStrategy implements MappingStrategy
{
    /**
     * @inheritDoc
     */
    public function getValue(string $propertyName, $source)
    {
        return $source->{'get' . ucfirst($propertyName)}();
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $propertyName, $target, $value): void
    {
        $target->{'set' . ucfirst($propertyName)}($value);
    }
}
