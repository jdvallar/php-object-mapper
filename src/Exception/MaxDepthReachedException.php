<?php


namespace Vallarj\ObjectMapper\Exception;


class MaxDepthReachedException extends Exception
{
}
