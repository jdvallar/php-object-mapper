<?php


namespace Vallarj\ObjectMapper\Exception;


class InvalidResolvedTargetObjectException extends Exception
{
}
