<?php


namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\LeafEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\SecondLevelEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\ThirdLevelEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\LeafEntity;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\SecondLevelEntity;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\ThirdLevelEntity;

class AbstractObjectMapperToOneAssociationTest extends AutoMapperTestCase
{
    /** @var LeafEntity */
    private $leafConstructor;

    /** @var LeafEntity */
    private $leafAccessor;

    /** @var SecondLevelEntity */
    private $secondLevelEntity;

    /** @var ThirdLevelEntity */
    private $thirdLevelEntity;

    private function getLeafForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper
        {
            protected $sourceClass = LeafEntity::class;
            protected $targetClass = LeafEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attrOne')
                    ->mapAttribute('attrConstructor');
            }
        };
    }

    private function getSecondLevelObjectForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper
        {
            protected $sourceClass = SecondLevelEntity::class;
            protected $targetClass = SecondLevelEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attr')
                    ->mapToOneAssociation('leafAccessor', LeafEntityDTO::class)
                    ->mapToOneAssociation('leafConstructor', LeafEntityDTO::class);
            }
        };
    }

    private function getThirdLevelObjectForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper
        {
            protected $sourceClass = ThirdLevelEntity::class;
            protected $targetClass = ThirdLevelEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attr')
                    ->mapToOneAssociation('secondLevel', SecondLevelEntityDTO::class)
                    ->mapToOneAssociation('secondLevelTwo', SecondLevelEntityDTO::class)
                    ->mapToOneAssociation('secondLevelThree', SecondLevelEntityDTO::class);
            }
        };
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register($this->getLeafForwardObjectMapper());
        $this->_autoMapper->register($this->getSecondLevelObjectForwardObjectMapper());
        $this->_autoMapper->register($this->getThirdLevelObjectForwardObjectMapper());

        $this->leafConstructor = new LeafEntity('constructor-asdf1234');
        $this->leafConstructor->setId('constructor-12345')
            ->setAttrOne(12);

        $this->leafAccessor = new LeafEntity('accessor-123');
        $this->leafAccessor->setId('accessor-12345')
            ->setAttrOne(13);

        $this->secondLevelEntity = new SecondLevelEntity($this->leafConstructor);
        $this->secondLevelEntity->setLeafAccessor($this->leafAccessor)
            ->setAttr('attr12345')
            ->setId('second-level-1');

        $this->thirdLevelEntity = new ThirdLevelEntity();
        $this->thirdLevelEntity->setSecondLevel($this->secondLevelEntity)
            ->setSecondLevelTwo($this->secondLevelEntity)
            ->setSecondLevelThree($this->secondLevelEntity)
            ->setAttr('attrSec1234')
            ->setId('third-level-1');
    }

    public function testShouldMapToOneAssociationsWithClassTarget(): void
    {
        /** @var SecondLevelEntityDTO $secondLevelDTO */
        $secondLevelDTO = $this->_autoMapper->map($this->secondLevelEntity, SecondLevelEntityDTO::class);

        $this->assertInstanceOf(SecondLevelEntityDTO::class, $secondLevelDTO);
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());

        // Leaf entity constructor check attributes
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafConstructorDTO);
        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());

        // Leaf entity accessor check attributes
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafAccessorDTO);
        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
    }

    public function testShouldMapToOneAssociationsWithObjectTarget(): void
    {
        $target = new SecondLevelEntityDTO();
        /** @var SecondLevelEntityDTO $secondLevelDTO */
        $secondLevelDTO = $this->_autoMapper->map($this->secondLevelEntity, $target);

        $this->assertSame($target, $secondLevelDTO);
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());

        // Leaf entity constructor check attributes
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafConstructorDTO);
        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());

        // Leaf entity accessor check attributes
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafAccessorDTO);
        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
    }

    public function testShouldMapAllToOneAssociationLevelsWhenPropertiesArrayIsNotSpecified(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map($this->thirdLevelEntity, ThirdLevelEntityDTO::class);

        $this->assertInstanceOf(ThirdLevelEntityDTO::class, $thirdLevelDTO);
        $this->assertEquals($this->thirdLevelEntity->getId(), $thirdLevelDTO->getId());
        $this->assertEquals($this->thirdLevelEntity->getAttr(), $thirdLevelDTO->getAttr());

        // Second level DTO checks
        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $this->assertInstanceOf(SecondLevelEntityDTO::class, $secondLevelDTO);
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());

        // Leaf entity constructor check attributes
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafConstructorDTO);
        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());

        // Leaf entity accessor check attributes
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $this->assertInstanceOf(LeafEntityDTO::class, $leafAccessorDTO);
        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
    }
}