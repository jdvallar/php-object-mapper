<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Vallarj\ObjectMapper\AutoMapper\Configuration;
use PHPUnit\Framework\TestCase;

abstract class AutoMapperTestCase extends TestCase
{
    /** @var AutoMapper */
    protected $_autoMapper;

    protected function _getDefaultTestAutoMapper(): AutoMapper
    {
        return new AutoMapper(new Configuration([Configuration::MAX_DEPTH => 6]));
    }

    protected function setUp(): void
    {
        if(!$this->_autoMapper) {
            $this->_autoMapper = $this->_getDefaultTestAutoMapper();
        }
    }
}