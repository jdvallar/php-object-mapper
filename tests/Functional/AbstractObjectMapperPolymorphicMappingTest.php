<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class AbstractObjectMapperPolymorphicMappingTest extends AutoMapperTestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register(new PolymorphicParentDTOForwardMapper());
        $this->_autoMapper->register(new PolymorphicEntityADTOForwardMapper());
        $this->_autoMapper->register(new PolymorphicEntityBDTOForwardMapper());
        $this->_autoMapper->register(new PolymorphicEntityCDTOForwardMapper());
    }

    public function testShouldMapPolymorphicTopLevelAutomapperCall(): void
    {
        $entityC = new PolymorphicEntityC();

        /** @var AbstractPolymorphicEntityDTO $result */
        $result = $this->_autoMapper->map($entityC, [
            PolymorphicEntityADTO::class,
            PolymorphicEntityBDTO::class,
            PolymorphicEntityCDTO::class
        ]);

        $this->assertInstanceOf(PolymorphicEntityCDTO::class, $result);
        $this->assertEquals($entityC->getId(), $result->getId());
        $this->assertEquals($entityC->getAttr(), $result->getAttr());
    }

    public function testShouldMapPolymorphicToOneAssociationsWithClassTarget(): void
    {
        $entityA = new PolymorphicEntityA();
        $entityB = new PolymorphicEntityB();
        $parent = new PolymorphicParent();

        $parent->setA($entityA)
            ->setB($entityB);

        /** @var PolymorphicParentDTO $result */
        $result = $this->_autoMapper->map($parent, PolymorphicParentDTO::class);
        $dtoA = $result->getA();
        $dtoB = $result->getB();

        $this->assertInstanceOf(PolymorphicEntityADTO::class, $dtoA);
        $this->assertEquals($entityA->getId(), $dtoA->getId());
        $this->assertEquals($entityA->getAttr(), $dtoA->getAttr());

        $this->assertInstanceOf(PolymorphicEntityBDTO::class, $dtoB);
        $this->assertEquals($entityB->getId(), $dtoB->getId());
        $this->assertEquals($entityB->getAttr(), $dtoB->getAttr());
    }

    public function testShouldMapPolymorphicToOneAssociationsWithObjectTarget(): void
    {
        $entityA = new PolymorphicEntityA();
        $entityB = new PolymorphicEntityB();
        $parent = new PolymorphicParent();

        $parent->setA($entityA)
            ->setB($entityB);

        $target = new PolymorphicParentDTO();

        /** @var PolymorphicParentDTO $result */
        $result = $this->_autoMapper->map($parent, $target);
        $dtoA = $result->getA();
        $dtoB = $result->getB();

        $this->assertSame($target, $result);

        $this->assertInstanceOf(PolymorphicEntityADTO::class, $dtoA);
        $this->assertEquals($entityA->getId(), $dtoA->getId());
        $this->assertEquals($entityA->getAttr(), $dtoA->getAttr());

        $this->assertInstanceOf(PolymorphicEntityBDTO::class, $dtoB);
        $this->assertEquals($entityB->getId(), $dtoB->getId());
        $this->assertEquals($entityB->getAttr(), $dtoB->getAttr());
    }

    public function testShouldMapPolymorphicToManyAssociationsWithClassTarget(): void
    {
        $entityA = new PolymorphicEntityA();
        $entityB = new PolymorphicEntityB();
        $entityC = new PolymorphicEntityC();
        $parent = new PolymorphicParent();

        $parent->addItem($entityA)
            ->addItem($entityB)
            ->addItem($entityC);

        /** @var PolymorphicParentDTO $result */
        $result = $this->_autoMapper->map($parent, PolymorphicParentDTO::class);

        $items = $result->getItems();
        $this->assertInstanceOf(PolymorphicEntityADTO::class, $items[0]);
        $this->assertEquals($entityA->getId(), $items[0]->getId());
        $this->assertEquals($entityA->getAttr(), $items[0]->getAttr());

        $this->assertInstanceOf(PolymorphicEntityBDTO::class, $items[1]);
        $this->assertEquals($entityB->getId(), $items[1]->getId());
        $this->assertEquals($entityB->getAttr(), $items[1]->getAttr());

        $this->assertInstanceOf(PolymorphicEntityCDTO::class, $items[2]);
        $this->assertEquals($entityC->getId(), $items[2]->getId());
        $this->assertEquals($entityC->getAttr(), $items[2]->getAttr());
    }

    public function testShouldMapPolymorphicToManyAssociationsWithObjectTarget(): void
    {
        $entityA = new PolymorphicEntityA();
        $entityB = new PolymorphicEntityB();
        $entityC = new PolymorphicEntityC();
        $parent = new PolymorphicParent();

        $parent->addItem($entityA)
            ->addItem($entityB)
            ->addItem($entityC);

        $target = new PolymorphicParentDTO();

        /** @var PolymorphicParentDTO $result */
        $result = $this->_autoMapper->map($parent, $target);

        $this->assertSame($target, $result);

        $items = $result->getItems();
        $this->assertInstanceOf(PolymorphicEntityADTO::class, $items[0]);
        $this->assertEquals($entityA->getId(), $items[0]->getId());
        $this->assertEquals($entityA->getAttr(), $items[0]->getAttr());

        $this->assertInstanceOf(PolymorphicEntityBDTO::class, $items[1]);
        $this->assertEquals($entityB->getId(), $items[1]->getId());
        $this->assertEquals($entityB->getAttr(), $items[1]->getAttr());

        $this->assertInstanceOf(PolymorphicEntityCDTO::class, $items[2]);
        $this->assertEquals($entityC->getId(), $items[2]->getId());
        $this->assertEquals($entityC->getAttr(), $items[2]->getAttr());
    }

    public function testShouldReuseObjectsWithSameClassAndIdentifier(): void
    {
        $entityA = new PolymorphicEntityA();
        $parent = new PolymorphicParent();

        $parent->setA($entityA)
            ->setB($entityA)
            ->addItem($entityA)
            ->addItem($entityA);

        /** @var PolymorphicParentDTO $result */
        $result = $this->_autoMapper->map($parent, PolymorphicParentDTO::class);

        $items = $result->getItems();

        $items[] = $result->getA();
        $items[] = $result->getB();

        foreach ($items as $item) {
            $this->assertInstanceOf(PolymorphicEntityADTO::class, $item);
            $this->assertSame($items[0], $item);
        }
    }
}

abstract class AbstractPolymorphicEntity
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $attr;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAttr(): string
    {
        return $this->attr;
    }

    /**
     * @param string $attr
     */
    public function setAttr(string $attr): void
    {
        $this->attr = $attr;
    }
}

class PolymorphicEntityA extends AbstractPolymorphicEntity
{
    protected $id = 'idA';
    protected $attr = 'attrA';
}

class PolymorphicEntityB extends AbstractPolymorphicEntity
{
    protected $id = 'idB';
    protected $attr = 'attrB';
}

class PolymorphicEntityC extends AbstractPolymorphicEntity
{
    protected $id = 'idC';
    protected $attr = 'attrC';
}

class PolymorphicParent
{
    /** @var string */
    private $id = 'parent';

    /** @var AbstractPolymorphicEntity|null */
    private $a;

    /** @var AbstractPolymorphicEntity|null */
    private $b;

    /** @var AbstractPolymorphicEntity[] */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return AbstractPolymorphicEntity|null
     */
    public function getA(): ?AbstractPolymorphicEntity
    {
        return $this->a;
    }

    /**
     * @param AbstractPolymorphicEntity|null $a
     * @return PolymorphicParent
     */
    public function setA(?AbstractPolymorphicEntity $a): PolymorphicParent
    {
        $this->a = $a;
        return $this;
    }

    /**
     * @return AbstractPolymorphicEntity|null
     */
    public function getB(): ?AbstractPolymorphicEntity
    {
        return $this->b;
    }

    /**
     * @param AbstractPolymorphicEntity|null $b
     * @return PolymorphicParent
     */
    public function setB(?AbstractPolymorphicEntity $b): PolymorphicParent
    {
        $this->b = $b;
        return $this;
    }

    /**
     * @return AbstractPolymorphicEntity[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param AbstractPolymorphicEntity $item
     * @return PolymorphicParent
     */
    public function addItem(AbstractPolymorphicEntity $item): PolymorphicParent
    {
        $this->items[] = $item;
        return $this;
    }
}

abstract class AbstractPolymorphicEntityDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attr;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAttr(): string
    {
        return $this->attr;
    }

    /**
     * @param string $attr
     */
    public function setAttr(string $attr): void
    {
        $this->attr = $attr;
    }
}

class PolymorphicEntityADTO extends AbstractPolymorphicEntityDTO
{

}

class PolymorphicEntityBDTO extends AbstractPolymorphicEntityDTO
{

}

class PolymorphicEntityCDTO extends AbstractPolymorphicEntityDTO
{

}

class PolymorphicParentDTO
{
    /** @var string */
    private $id;

    /** @var AbstractPolymorphicEntityDTO|null */
    private $a;

    /** @var AbstractPolymorphicEntityDTO|null */
    private $b;

    /** @var AbstractPolymorphicEntityDTO[] */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return PolymorphicParentDTO
     */
    public function setId(string $id): PolymorphicParentDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return AbstractPolymorphicEntityDTO|null
     */
    public function getA(): ?AbstractPolymorphicEntityDTO
    {
        return $this->a;
    }

    /**
     * @param AbstractPolymorphicEntityDTO|null $a
     * @return PolymorphicParentDTO
     */
    public function setA(?AbstractPolymorphicEntityDTO $a): PolymorphicParentDTO
    {
        $this->a = $a;
        return $this;
    }

    /**
     * @return AbstractPolymorphicEntityDTO|null
     */
    public function getB(): ?AbstractPolymorphicEntityDTO
    {
        return $this->b;
    }

    /**
     * @param AbstractPolymorphicEntityDTO|null $b
     * @return PolymorphicParentDTO
     */
    public function setB(?AbstractPolymorphicEntityDTO $b): PolymorphicParentDTO
    {
        $this->b = $b;
        return $this;
    }

    /**
     * @return AbstractPolymorphicEntityDTO[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param AbstractPolymorphicEntityDTO $item
     * @return PolymorphicParentDTO
     */
    public function addItem(AbstractPolymorphicEntityDTO $item): PolymorphicParentDTO
    {
        $this->items[] = $item;
        return $this;
    }
}

class PolymorphicEntityADTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = PolymorphicEntityA::class;
    protected $targetClass = PolymorphicEntityADTO::class;

    public function __construct()
    {
        $this->mapAttribute('attr');
    }
}

class PolymorphicEntityBDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = PolymorphicEntityB::class;
    protected $targetClass = PolymorphicEntityBDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attr');
    }
}

class PolymorphicEntityCDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = PolymorphicEntityC::class;
    protected $targetClass = PolymorphicEntityCDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attr');
    }
}

class PolymorphicParentDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = PolymorphicParent::class;
    protected $targetClass = PolymorphicParentDTO::class;

    public function __construct()
    {
        $this->mapToOneAssociation('a', [
            PolymorphicEntityADTO::class,
            PolymorphicEntityBDTO::class,
            PolymorphicEntityCDTO::class,
        ])->mapToOneAssociation('b', [
            PolymorphicEntityADTO::class,
            PolymorphicEntityBDTO::class,
            PolymorphicEntityCDTO::class,
        ])->mapToManyAssociation('items', [
            PolymorphicEntityADTO::class,
            PolymorphicEntityBDTO::class,
            PolymorphicEntityCDTO::class,
        ]);
    }
}