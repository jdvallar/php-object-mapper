<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\LeafEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\SecondLevelEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\LeafEntity;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\SecondLevelEntity;

class AbstractObjectMapperMappingOverridesTest extends AutoMapperTestCase
{
    /** @var LeafReverseObjectMapper  */
    private $leafReverseObjectMapper;

    /** @var SecondLevelReverseObjectMapper */
    private $secondLevelReverseObjectMapper;

    protected function setUp(): void
    {
        parent::setUp();

        $this->leafReverseObjectMapper = new LeafReverseObjectMapper();
        $this->secondLevelReverseObjectMapper = new SecondLevelReverseObjectMapper();

        $this->_autoMapper->register($this->leafReverseObjectMapper);
        $this->_autoMapper->register($this->secondLevelReverseObjectMapper);
    }

    public function testShouldUseCreateTargetObjectOverrideOnClassTarget(): void
    {
        $leafEntityDTO = new LeafEntityDTO();
        $leafEntityDTO->setId('leaf-id')
            ->setAttrOne(1)
            ->setAttrConstructor('set-from-dto');

        $leafEntity = $this->_autoMapper->map($leafEntityDTO, LeafEntity::class);
        $this->assertSame($this->leafReverseObjectMapper->createdTargetObject, $leafEntity);
    }

    public function testShouldReturnSpecifiedTargetObjectOnObjectTarget(): void
    {
        $leafEntityDTO = new LeafEntityDTO();
        $leafEntityDTO->setId('leaf-id')
            ->setAttrOne(1)
            ->setAttrConstructor('set-from-dto');

        $target = new LeafEntity("attr");
        $leafEntity = $this->_autoMapper->map($leafEntityDTO, $target);
        $this->assertSame($target, $leafEntity);
    }

    public function testShouldUseResolveTargetOnAssociationOverrideOnToOneAssociationMapping(): void
    {
        $leafEntityDTO = new LeafEntityDTO();
        $leafEntityDTO->setId('leaf-id')
            ->setAttrOne(1)
            ->setAttrConstructor('set-from-dto');

        $secondLevelDTO = new SecondLevelEntityDTO();
        $secondLevelDTO->setId('second-level-id')
            ->setAttr('attr')
            ->setLeafConstructor($leafEntityDTO)
            ->setLeafAccessor($leafEntityDTO);

        /** @var SecondLevelEntity $secondLevelEntity */
        $secondLevelEntity = $this->_autoMapper->map($secondLevelDTO, SecondLevelEntity::class);

        $this->assertSame(
            $this->leafReverseObjectMapper->resolvedAssocLeaf,
            $secondLevelEntity->getLeafAccessor()
        );
    }

    public function testShouldNotMapToOneAssociationOnCreateWhenMapOnCreateIsFalse(): void
    {
        $leafEntityDTO = new LeafEntityDTO();
        $leafEntityDTO->setId('leaf-id')
            ->setAttrOne(1)
            ->setAttrConstructor('set-from-dto');

        $secondLevelDTO = new SecondLevelEntityDTO();
        $secondLevelDTO->setId('second-level-id')
            ->setAttr('attr')
            ->setLeafConstructor($leafEntityDTO)
            ->setLeafAccessor($leafEntityDTO);

        /** @var SecondLevelEntity $secondLevelEntity */
        $secondLevelEntity = $this->_autoMapper->map($secondLevelDTO, SecondLevelEntity::class);

        $this->assertSame(
            $this->secondLevelReverseObjectMapper->resolvedLeafConstructor,
            $secondLevelEntity->getLeafConstructor()
        );

        $this->assertNotEquals(
            $secondLevelDTO->getLeafConstructor()->getAttrConstructor(),
            $secondLevelEntity->getLeafConstructor()->getAttrConstructor()
        );
    }

    public function testShouldNotMapAttributeOnCreateWhenMapOnCreateIsFalse(): void
    {
        $leafEntityDTO = new LeafEntityDTO();
        $leafEntityDTO->setId('leaf-id')
            ->setAttrOne(1)
            ->setAttrConstructor('set-from-dto');

        /** @var LeafEntity $leafEntity */
        $leafEntity = $this->_autoMapper->map($leafEntityDTO, LeafEntity::class);

        $this->assertNotEquals(
            $leafEntityDTO->getAttrConstructor(),
            $leafEntity->getAttrConstructor()
        );
    }
}

class LeafReverseObjectMapper extends AbstractObjectMapper
{
    protected $sourceClass = LeafEntityDTO::class;
    protected $targetClass = LeafEntity::class;

    /** @var LeafEntity */
    public $resolvedAssocLeaf;

    /** @var LeafEntity */
    public $createdTargetObject;

    public function __construct()
    {
        $this->resolvedAssocLeaf = new LeafEntity("attrItem");
        $this->createdTargetObject = null;

        $this->mapAttribute('attrOne')
            ->mapAttribute('attrConstructor', false);
    }

    /**
     * @param AutoMapperInterface $autoMapper
     * @param LeafEntityDTO $source
     * @param string $targetClass
     * @param Context $context
     * @return LeafEntity
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        $this->createdTargetObject = new LeafEntity('attr-constructor-must-not-be-equal');
        return $this->createdTargetObject;
    }

    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        return $this->resolvedAssocLeaf;
    }
}

class SecondLevelReverseObjectMapper extends AbstractObjectMapper
{
    protected $sourceClass = SecondLevelEntityDTO::class;
    protected $targetClass = SecondLevelEntity::class;

    /** @var LeafEntity */
    public $resolvedLeafConstructor;

    /** @var SecondLevelEntity */
    public $createdTargetObject;

    public function __construct()
    {
        $this->mapAttribute('attr')
            ->mapToOneAssociation('leafAccessor', LeafEntity::class)
            ->mapToOneAssociation('leafConstructor', LeafEntity::class, false);

        $this->resolvedLeafConstructor = new LeafEntity('resolvedConstructor');
        $this->createdTargetObject = null;
    }

    /**
     * @param AutoMapperInterface $autoMapper
     * @param SecondLevelEntityDTO $source
     * @param string $targetClass
     * @param Context $context
     * @return SecondLevelEntity
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        $this->createdTargetObject = new SecondLevelEntity($this->resolvedLeafConstructor);
        return $this->createdTargetObject;
    }
}