<?php


namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\LeafEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\SecondLevelEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\ThirdLevelEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\LeafEntity;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\SecondLevelEntity;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\ThirdLevelEntity;

class AbstractObjectMapperSelectedPropertyMappingTest extends AutoMapperTestCase
{
    /** @var LeafEntity */
    private $leafConstructor;

    /** @var LeafEntity */
    private $leafAccessor;

    /** @var SecondLevelEntity */
    private $secondLevelEntity;

    /** @var ThirdLevelEntity */
    private $thirdLevelEntity;

    private function getLeafForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper {
            protected $sourceClass = LeafEntity::class;
            protected $targetClass = LeafEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attrOne')
                    ->mapAttribute('attrConstructor');
            }
        };
    }

    private function getSecondLevelObjectForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper {
            protected $sourceClass = SecondLevelEntity::class;
            protected $targetClass = SecondLevelEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attr')
                    ->mapToOneAssociation('leafAccessor', LeafEntityDTO::class)
                    ->mapToOneAssociation('leafConstructor', LeafEntityDTO::class);
            }
        };
    }

    private function getThirdLevelObjectForwardObjectMapper(): AbstractObjectMapper
    {
        return new class extends AbstractObjectMapper {
            protected $sourceClass = ThirdLevelEntity::class;
            protected $targetClass = ThirdLevelEntityDTO::class;

            public function __construct()
            {
                $this->mapAttribute('attr')
                    ->mapToOneAssociation('secondLevel', SecondLevelEntityDTO::class)
                    ->mapToOneAssociation('secondLevelTwo', SecondLevelEntityDTO::class)
                    ->mapToOneAssociation('secondLevelThree', SecondLevelEntityDTO::class);
            }
        };
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register($this->getLeafForwardObjectMapper());
        $this->_autoMapper->register($this->getSecondLevelObjectForwardObjectMapper());
        $this->_autoMapper->register($this->getThirdLevelObjectForwardObjectMapper());

        $this->leafConstructor = new LeafEntity('constructor-asdf1234');
        $this->leafConstructor->setId('constructor-12345')
            ->setAttrOne(12);

        $this->leafAccessor = new LeafEntity('accessor-123');
        $this->leafAccessor->setId('accessor-12345')
            ->setAttrOne(13);

        $this->secondLevelEntity = new SecondLevelEntity($this->leafConstructor);
        $this->secondLevelEntity->setLeafAccessor($this->leafAccessor)
            ->setAttr('attr12345')
            ->setId('second-level-1');

        $this->thirdLevelEntity = new ThirdLevelEntity();
        $this->thirdLevelEntity->setSecondLevel($this->secondLevelEntity)
            ->setSecondLevelTwo($this->secondLevelEntity)
            ->setSecondLevelThree($this->secondLevelEntity)
            ->setAttr('attrSec1234')
            ->setId('third-level-1');
    }

    public function testShouldMapSelectedProperties(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'id',
                'attr',
                'secondLevel'
            ]
        );

        $this->assertEquals($this->thirdLevelEntity->getId(), $thirdLevelDTO->getId());
        $this->assertEquals($this->thirdLevelEntity->getAttr(), $thirdLevelDTO->getAttr());
        $this->assertInstanceOf(SecondLevelEntityDTO::class, $thirdLevelDTO->getSecondLevel());
    }

    public function testShouldNotMapUnselectedProperties(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            []
        );

        $this->assertNull($thirdLevelDTO->getId());
        $this->assertNull($thirdLevelDTO->getAttr());
        $this->assertNull($thirdLevelDTO->getSecondLevel());
    }

    public function testShouldOnlyMapAssociationsIdsIfAssociationAttributesAreNotSelected(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel'
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();

        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertNull($secondLevelDTO->getAttr());
        $this->assertNull($secondLevelDTO->getLeafAccessor());
        $this->assertNull($secondLevelDTO->getLeafConstructor());

        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevel.leafAccessor',
                'secondLevel.leafConstructor'
            ]
        );

        $leafAccessorDTO = $thirdLevelDTO->getSecondLevel()->getLeafAccessor();
        $leafConstructorDTO = $thirdLevelDTO->getSecondLevel()->getLeafConstructor();

        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertNull($leafAccessorDTO->getAttrConstructor());
        $this->assertNull($leafAccessorDTO->getAttrOne());

        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertNull($leafConstructorDTO->getAttrConstructor());
        $this->assertNull($leafConstructorDTO->getAttrOne());
    }

    public function testShouldMapSpecifiedAttributesOfNestedAssociations(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevel.attr',
                'secondLevel.leafAccessor',
                'secondLevel.leafAccessor.attrOne',
                'secondLevel.leafAccessor.attrConstructor',
                'secondLevel.leafConstructor',
                'secondLevel.leafConstructor.attrOne',
                'secondLevel.leafConstructor.attrConstructor',
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();

        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
        $this->assertEquals($this->leafAccessor->getAttrConstructor(), $leafAccessorDTO->getAttrConstructor());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());
        $this->assertEquals($this->leafConstructor->getAttrConstructor(), $leafConstructorDTO->getAttrConstructor());
    }

    public function testShouldMapAllTopLevelPropertiesOnTopLevelWildcard(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                '*'
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();

        $this->assertEquals($this->thirdLevelEntity->getId(), $thirdLevelDTO->getId());
        $this->assertEquals($this->thirdLevelEntity->getAttr(), $thirdLevelDTO->getAttr());
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
    }

    public function testShouldNotMapAssociationPropertiesOnAHigherLevelWildcard(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                '*'
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();

        $this->assertNull($secondLevelDTO->getAttr());
        $this->assertNull($secondLevelDTO->getLeafConstructor());
        $this->assertNull($secondLevelDTO->getLeafAccessor());

        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevel.*'
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();

        $this->assertNull($leafAccessorDTO->getAttrOne());
        $this->assertNull($leafAccessorDTO->getAttrConstructor());
        $this->assertNull($leafConstructorDTO->getAttrOne());
        $this->assertNull($leafConstructorDTO->getAttrConstructor());
    }

    public function testShouldMapAllPropertiesOfSpecifiedAssociationsOnWildcard(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevel.*',
                'secondLevel.leafAccessor',
                'secondLevel.leafAccessor.*',
                'secondLevel.leafConstructor',
                'secondLevel.leafConstructor.*',
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();

        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());
        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
        $this->assertEquals($this->leafAccessor->getAttrConstructor(), $leafAccessorDTO->getAttrConstructor());
        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());
        $this->assertEquals($this->leafConstructor->getAttrConstructor(), $leafConstructorDTO->getAttrConstructor());
    }

    public function testShouldUpdateObjectCacheIfSameLevelAssociationsHaveDifferentIncludedAttributes(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevelTwo',
                'secondLevelTwo.*',
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $secondLevelTwoDTO = $thirdLevelDTO->getSecondLevelTwo();

        $this->assertSame($secondLevelDTO, $secondLevelTwoDTO);
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());
    }

    public function testShouldUpdateObjectCacheIfSameLevelAssociationsHaveDifferentIncludedRelationships(): void
    {
        /** @var ThirdLevelEntityDTO $thirdLevelDTO */
        $thirdLevelDTO = $this->_autoMapper->map(
            $this->thirdLevelEntity,
            ThirdLevelEntityDTO::class,
            [
                'secondLevel',
                'secondLevel.*',
                'secondLevelTwo',
                'secondLevelTwo.leafAccessor',
                'secondLevelTwo.leafAccessor.*',
                'secondLevelThree',
                'secondLevelThree.leafConstructor',
                'secondLevelThree.leafConstructor.*',
            ]
        );

        $secondLevelDTO = $thirdLevelDTO->getSecondLevel();
        $leafAccessorDTO = $secondLevelDTO->getLeafAccessor();
        $leafConstructorDTO = $secondLevelDTO->getLeafConstructor();

        $this->assertSame($secondLevelDTO, $thirdLevelDTO->getSecondLevelTwo());
        $this->assertSame($secondLevelDTO, $thirdLevelDTO->getSecondLevelThree());
        $this->assertEquals($this->secondLevelEntity->getId(), $secondLevelDTO->getId());
        $this->assertEquals($this->secondLevelEntity->getAttr(), $secondLevelDTO->getAttr());
        $this->assertEquals($this->leafAccessor->getId(), $leafAccessorDTO->getId());
        $this->assertEquals($this->leafAccessor->getAttrOne(), $leafAccessorDTO->getAttrOne());
        $this->assertEquals($this->leafAccessor->getAttrConstructor(), $leafAccessorDTO->getAttrConstructor());
        $this->assertEquals($this->leafConstructor->getId(), $leafConstructorDTO->getId());
        $this->assertEquals($this->leafConstructor->getAttrOne(), $leafConstructorDTO->getAttrOne());
        $this->assertEquals($this->leafConstructor->getAttrConstructor(), $leafConstructorDTO->getAttrConstructor());
    }
}
