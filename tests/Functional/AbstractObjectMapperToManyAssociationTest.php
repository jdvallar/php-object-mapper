<?php


namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\LeafEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\LeafEntity;

class AbstractObjectMapperToManyAssociationTest extends AutoMapperTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register(new LeafForwardObjectMapper());
        $this->_autoMapper->register(new ToManyAssocDTOForwardMapper());
        $this->_autoMapper->register(new TopLevelEntityDTOForwardMapper());
    }

    public function testShouldMapToManyAssociationsWithClassTarget(): void
    {
        $parent = new ToManyAssocEntity();
        $childA = new LeafEntity('attrCA');
        $childA->setAttrOne(1)
            ->setId('id-a');

        $childB = new LeafEntity('attrCB');
        $childB->setAttrOne(2)
            ->setId('id-b');

        $parent->setId('id-parent')
            ->setAttr('attr-parent')
            ->addLeafEntity($childA)
            ->addLeafEntity($childB);

        /** @var ToManyAssocDTO $result */
        $result = $this->_autoMapper->map($parent, ToManyAssocDTO::class);

        $this->assertInstanceOf(ToManyAssocDTO::class, $result);
        $this->assertEquals($parent->getId(), $result->getId());
        $this->assertEquals($parent->getAttr(), $result->getAttr());

        $parentChildren = $parent->getLeafEntities();
        $resultChildren = $result->getLeafEntities();
        $parentCount = count($parentChildren);
        $this->assertCount($parentCount, $resultChildren);

        for ($i = 0; $i < $parentCount; $i++) {
            $parentChild = $parentChildren[$i];
            $resultChild = $resultChildren[$i];
            $this->assertEquals($parentChild->getId(), $resultChild->getId());
            $this->assertEquals($parentChild->getAttrOne(), $resultChild->getAttrOne());
            $this->assertEquals($parentChild->getAttrConstructor(), $resultChild->getAttrConstructor());
        }
    }

    public function testShouldMapToManyAssociationsWithObjectTarget(): void
    {
        $parent = new ToManyAssocEntity();
        $childA = new LeafEntity('attrCA');
        $childA->setAttrOne(1)
            ->setId('id-a');

        $childB = new LeafEntity('attrCB');
        $childB->setAttrOne(2)
            ->setId('id-b');

        $parent->setId('id-parent')
            ->setAttr('attr-parent')
            ->addLeafEntity($childA)
            ->addLeafEntity($childB);

        $target = new ToManyAssocDTO();

        /** @var ToManyAssocDTO $result */
        $result = $this->_autoMapper->map($parent, $target);

        $this->assertSame($target, $result);

        $parentChildren = $parent->getLeafEntities();
        $resultChildren = $result->getLeafEntities();
        $parentCount = count($parentChildren);
        $this->assertCount($parentCount, $resultChildren);

        for ($i = 0; $i < $parentCount; $i++) {
            $parentChild = $parentChildren[$i];
            $resultChild = $resultChildren[$i];
            $this->assertEquals($parentChild->getId(), $resultChild->getId());
            $this->assertEquals($parentChild->getAttrOne(), $resultChild->getAttrOne());
            $this->assertEquals($parentChild->getAttrConstructor(), $resultChild->getAttrConstructor());
        }
    }

    public function testShouldReuseObjectsWithSameClassAndIdentifier(): void
    {
        $parent = new ToManyAssocEntity();
        $childA = new LeafEntity('attrCA');
        $childA->setAttrOne(1)
            ->setId('id-a');

        $parent->setId('id-parent')
            ->setAttr('attr-parent')
            ->addLeafEntity($childA)
            ->addLeafEntity($childA)
            ->addLeafEntity($childA);

        /** @var ToManyAssocDTO $result */
        $result = $this->_autoMapper->map($parent, ToManyAssocDTO::class);

        $parentChildren = $parent->getLeafEntities();
        $resultChildren = $result->getLeafEntities();
        $parentCount = count($parentChildren);
        $this->assertCount($parentCount, $resultChildren);

        foreach ($resultChildren as $resultChild) {
            $this->assertSame($resultChildren[0], $resultChild);
        }
    }

    public function testShouldMapAllToManyAssociationLevelsWhenPropertiesArrayIsNotSpecified(): void
    {
        $topLevel = new TopLevelEntity();
        $toManyAssocA = new ToManyAssocEntity();
        $toManyAssocB = new ToManyAssocEntity();
        $leafA = new LeafEntity('attrLA');
        $leafA->setAttrOne(1)
            ->setId('id-a');

        $leafB = new LeafEntity('attrLB');
        $leafB->setAttrOne(2)
            ->setId('id-b');

        $leafC = new LeafEntity('attrLC');
        $leafC->setAttrOne(3)
            ->setId('id-c');

        $leafD = new LeafEntity('attrLD');
        $leafD->setAttrOne(4)
            ->setId('id-d');

        $topLevel->setId('TL1')
            ->addToManyAssoc($toManyAssocA)
            ->addToManyAssoc($toManyAssocB);

        $toManyAssocA->setId('TM1')
            ->setAttr('ATTR_TM1')
            ->addLeafEntity($leafA)
            ->setSingleLeaf($leafC);

        $toManyAssocB->setId('TM2')
            ->setAttr('ATTR_TM2')
            ->addLeafEntity($leafB)
            ->setSingleLeaf($leafD);

        /** @var TopLevelEntityDTO $result */
        $result = $this->_autoMapper->map($topLevel, TopLevelEntityDTO::class);

        $toManyAssocs = [
            $toManyAssocA,
            $toManyAssocB
        ];

        $leaves = [
            $leafA,
            $leafB
        ];

        $singleLeaves = [
            $leafC,
            $leafD
        ];

        $toManyAssocsDTO = $result->getToManyAssocs();
        $i = 0;
        foreach ($toManyAssocsDTO as $toManyAssocDTO) {
            $this->assertEquals($toManyAssocs[$i]->getId(), $toManyAssocDTO->getId());
            $this->assertEquals($toManyAssocs[$i]->getAttr(), $toManyAssocDTO->getAttr());

            $leaf = $toManyAssocDTO->getLeafEntities()[0];
            $singleLeaf = $toManyAssocDTO->getSingleLeaf();
            $this->assertNotNull($leaf->getId());
            $this->assertEquals($leaves[$i]->getId(), $leaf->getId());
            $this->assertEquals($leaves[$i]->getAttrConstructor(), $leaf->getAttrConstructor());
            $this->assertEquals($leaves[$i]->getAttrOne(), $leaf->getAttrOne());
            $this->assertNotNull($singleLeaf->getId());
            $this->assertEquals($singleLeaves[$i]->getId(), $singleLeaf->getId());
            $this->assertEquals($singleLeaves[$i]->getAttrConstructor(), $singleLeaf->getAttrConstructor());
            $this->assertEquals($singleLeaves[$i]->getAttrOne(), $singleLeaf->getAttrOne());

            $i++;
        }
    }
}

class TopLevelEntity
{
    /** @var string */
    private $id;

    /** @var ToManyAssocEntity[] */
    private $toManyAssocs;

    /**
     * TopLevelEntity constructor.
     */
    public function __construct()
    {
        $this->toManyAssocs = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return TopLevelEntity
     */
    public function setId(string $id): TopLevelEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ToManyAssocEntity[]
     */
    public function getToManyAssocs(): array
    {
        return $this->toManyAssocs;
    }

    /**
     * @param ToManyAssocEntity $toManyAssoc
     * @return TopLevelEntity
     */
    public function addToManyAssoc(ToManyAssocEntity $toManyAssoc): TopLevelEntity
    {
        $this->toManyAssocs[] = $toManyAssoc;
        return $this;
    }
}

class TopLevelEntityDTO
{
    /** @var string */
    private $id;

    /** @var ToManyAssocDTO[] */
    private $toManyAssocs;

    /**
     * TopLevelEntityDTO constructor.
     */
    public function __construct()
    {
        $this->toManyAssocs = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return TopLevelEntityDTO
     */
    public function setId(string $id): TopLevelEntityDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ToManyAssocDTO[]
     */
    public function getToManyAssocs(): array
    {
        return $this->toManyAssocs;
    }

    /**
     * @param ToManyAssocDTO $toManyAssoc
     * @return TopLevelEntityDTO
     */
    public function addToManyAssoc(ToManyAssocDTO $toManyAssoc): TopLevelEntityDTO
    {
        $this->toManyAssocs[] = $toManyAssoc;
        return $this;
    }
}

class ToManyAssocEntity
{
    /** @var string */
    private $id;

    /** @var string */
    private $attr;

    /** @var LeafEntity[] */
    private $leafEntities;

    /** @var LeafEntity|null */
    private $singleLeaf;

    public function __construct()
    {
        $this->leafEntities = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ToManyAssocEntity
     */
    public function setId(string $id): ToManyAssocEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttr(): string
    {
        return $this->attr;
    }

    /**
     * @param string $attr
     * @return ToManyAssocEntity
     */
    public function setAttr(string $attr): ToManyAssocEntity
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return LeafEntity[]
     */
    public function getLeafEntities(): array
    {
        return $this->leafEntities;
    }

    /**
     * @param LeafEntity $leafEntity
     * @return ToManyAssocEntity
     */
    public function addLeafEntity(LeafEntity $leafEntity): ToManyAssocEntity
    {
        $this->leafEntities[] = $leafEntity;
        return $this;
    }

    /**
     * @return LeafEntity|null
     */
    public function getSingleLeaf(): ?LeafEntity
    {
        return $this->singleLeaf;
    }

    /**
     * @param LeafEntity|null $singleLeaf
     * @return ToManyAssocEntity
     */
    public function setSingleLeaf(?LeafEntity $singleLeaf): ToManyAssocEntity
    {
        $this->singleLeaf = $singleLeaf;
        return $this;
    }
}

class ToManyAssocDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attr;

    /** @var LeafEntityDTO[] */
    private $leafEntities;

    /** @var LeafEntityDTO|null */
    private $singleLeaf;

    public function __construct()
    {
        $this->leafEntities = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ToManyAssocDTO
     */
    public function setId(string $id): ToManyAssocDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttr(): string
    {
        return $this->attr;
    }

    /**
     * @param string $attr
     * @return ToManyAssocDTO
     */
    public function setAttr(string $attr): ToManyAssocDTO
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return LeafEntityDTO[]
     */
    public function getLeafEntities(): array
    {
        return $this->leafEntities;
    }

    /**
     * @param LeafEntityDTO $leafEntityDTO
     * @return ToManyAssocDTO
     */
    public function addLeafEntity(LeafEntityDTO $leafEntityDTO): ToManyAssocDTO
    {
        $this->leafEntities[] = $leafEntityDTO;
        return $this;
    }

    /**
     * @return LeafEntityDTO|null
     */
    public function getSingleLeaf(): ?LeafEntityDTO
    {
        return $this->singleLeaf;
    }

    /**
     * @param LeafEntityDTO|null $singleLeaf
     * @return ToManyAssocDTO
     */
    public function setSingleLeaf(?LeafEntityDTO $singleLeaf): ToManyAssocDTO
    {
        $this->singleLeaf = $singleLeaf;
        return $this;
    }
}

class TopLevelEntityDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = TopLevelEntity::class;
    protected $targetClass = TopLevelEntityDTO::class;

    public function __construct()
    {
        $this->mapToManyAssociation('toManyAssocs', ToManyAssocDTO::class);
    }
}

class ToManyAssocDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = ToManyAssocEntity::class;
    protected $targetClass = ToManyAssocDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attr')
            ->mapToManyAssociation('leafEntities', LeafEntityDTO::class)
            ->mapToOneAssociation('singleLeaf', LeafEntityDTO::class);
    }
}

class LeafForwardObjectMapper extends AbstractObjectMapper
{
    protected $sourceClass = LeafEntity::class;
    protected $targetClass = LeafEntityDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrOne')
            ->mapAttribute('attrConstructor');
    }
}