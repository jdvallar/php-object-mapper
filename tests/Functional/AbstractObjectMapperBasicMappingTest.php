<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Tests\Functional\Models\DTO\BasicGetterSetterEntityDTO;
use Vallarj\ObjectMapper\Tests\Functional\Models\Entity\BasicGetterSetterEntity;

class AbstractObjectMapperBasicMappingTest extends AutoMapperTestCase
{
    /** @var BasicGetterSetterEntity */
    private $sourceObject;

    private static $mappedAttributes = [
        'attrOne',
        'attrTwo'
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register(new class(self::$mappedAttributes) extends AbstractObjectMapper
        {
            protected $sourceClass = BasicGetterSetterEntity::class;
            protected $targetClass = BasicGetterSetterEntityDTO::class;

            public function __construct($mappedAttributes)
            {
                foreach($mappedAttributes as $attribute) {
                    $this->mapAttribute($attribute);
                }
            }
        });
        $this->_autoMapper->register(new AOMBasicDTOReverseMapper());

        $this->sourceObject = new BasicGetterSetterEntity();
        $this->sourceObject->setId("test-id")
            ->setAttrOne(1)
            ->setAttrTwo("two");
    }

    public function testShouldNotMapIdentifierWhenMapOnCreateIsSetToFalse(): void
    {
        $aomDTO = new AOMBasicDTO();
        $aomDTO->setId('1234')
            ->setAttr('attr');

        /** @var AOMBasicEntity $aomEntity */
        $aomEntity = $this->_autoMapper->map($aomDTO, AOMBasicEntity::class);
        $this->assertNull($aomEntity->getId());
    }

    public function testShouldNotMapAttributeWhenMapOnCreateIsSetToFalse(): void
    {
        $aomDTO = new AOMBasicDTO();
        $aomDTO->setId('1234')
            ->setAttr('attr');

        /** @var AOMBasicEntity $aomEntity */
        $aomEntity = $this->_autoMapper->map($aomDTO, AOMBasicEntity::class);
        $this->assertNull($aomEntity->getAttr());
    }

    public function testReturnedObjectShouldBeAnInstanceOfTheTargetClassName(): void
    {
        $result = $this->_autoMapper->map($this->sourceObject, BasicGetterSetterEntityDTO::class);
        $this->assertInstanceOf(BasicGetterSetterEntityDTO::class, $result);
    }

    public function testReturnedObjectShouldBeTheSameInstanceAsTargetObject(): void
    {
        $target = new BasicGetterSetterEntityDTO();
        $result = $this->_autoMapper->map($this->sourceObject, $target);
        $this->assertSame($result, $target);
    }

    public function testShouldMapAttributesWithTargetClassName(): void
    {
        $dto = $this->_autoMapper->map($this->sourceObject, BasicGetterSetterEntityDTO::class);
        foreach(self::$mappedAttributes as $attribute) {
            $this->assertEquals(
                $this->sourceObject->{'get' . ucfirst($attribute)}(),
                $dto->{'get' . ucfirst($attribute)}()
            );
        }
    }

    public function testShouldMapIdentifierWithTargetClassName(): void
    {
        $dto = $this->_autoMapper->map($this->sourceObject, BasicGetterSetterEntityDTO::class);
        $this->assertEquals($this->sourceObject->getId(), $dto->getId());
    }

    public function testShouldMapAttributesWithTargetObject(): void
    {
        $dto = $this->_autoMapper->map($this->sourceObject, new BasicGetterSetterEntityDTO());
        foreach(self::$mappedAttributes as $attribute) {
            $this->assertEquals(
                $this->sourceObject->{'get' . ucfirst($attribute)}(),
                $dto->{'get' . ucfirst($attribute)}()
            );
        }
    }

    public function testShouldMapIdentifierWithTargetObject(): void
    {
        $dto = $this->_autoMapper->map($this->sourceObject, new BasicGetterSetterEntityDTO());
        $this->assertEquals($this->sourceObject->getId(), $dto->getId());
    }

    public function testShouldMapNullSourceAsNull()
    {
        $this->assertNull($this->_autoMapper->map(null, null));
        $this->assertNull($this->_autoMapper->map(null, new BasicGetterSetterEntityDTO()));
        $this->assertNull($this->_autoMapper->map(null, BasicGetterSetterEntityDTO::class));
    }
}

class AOMBasicEntity
{
    /** @var string|null */
    private $id;

    /** @var string|null */
    private $attr;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return AOMBasicEntity
     */
    public function setId(?string $id): AOMBasicEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttr(): ?string
    {
        return $this->attr;
    }

    /**
     * @param string|null $attr
     * @return AOMBasicEntity
     */
    public function setAttr(?string $attr): AOMBasicEntity
    {
        $this->attr = $attr;
        return $this;
    }
}

class AOMBasicDTO
{
    /** @var string|null */
    private $id;

    /** @var string|null */
    private $attr;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return AOMBasicDTO
     */
    public function setId(?string $id): AOMBasicDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttr(): ?string
    {
        return $this->attr;
    }

    /**
     * @param string|null $attr
     * @return AOMBasicDTO
     */
    public function setAttr(?string $attr): AOMBasicDTO
    {
        $this->attr = $attr;
        return $this;
    }
}

class AOMBasicDTOReverseMapper extends AbstractObjectMapper
{
    protected $sourceClass = AOMBasicDTO::class;
    protected $targetClass = AOMBasicEntity::class;

    /**
     * AOMBasicDTOReverseMapper constructor.
     */
    public function __construct()
    {
        $this->setIdentifier('id', false)
            ->mapAttribute('attr', false);
    }

    /**
     * @param AutoMapperInterface $autoMapper
     * @param mixed $source
     * @param string $targetClass
     * @param Context $context
     * @return mixed
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        return new AOMBasicEntity();
    }
}
