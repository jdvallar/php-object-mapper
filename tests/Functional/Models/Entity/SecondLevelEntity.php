<?php


namespace Vallarj\ObjectMapper\Tests\Functional\Models\Entity;


class SecondLevelEntity
{
    /** @var string */
    private $id;

    /** @var string */
    private $attr;

    /** @var LeafEntity */
    private $leafAccessor;

    /** @var LeafEntity */
    private $leafConstructor;

    public function __construct(LeafEntity $leafConstructor)
    {
        $this->leafConstructor = $leafConstructor;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return SecondLevelEntity
     */
    public function setId(string $id): SecondLevelEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttr(): string
    {
        return $this->attr;
    }

    /**
     * @param string $attr
     * @return SecondLevelEntity
     */
    public function setAttr(string $attr): SecondLevelEntity
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return LeafEntity
     */
    public function getLeafConstructor(): LeafEntity
    {
        return $this->leafConstructor;
    }

    /**
     * @param LeafEntity $leafConstructor
     * @return SecondLevelEntity
     */
    public function setLeafConstructor(LeafEntity $leafConstructor): SecondLevelEntity
    {
        $this->leafConstructor = $leafConstructor;
        return $this;
    }

    /**
     * @return LeafEntity
     */
    public function getLeafAccessor(): LeafEntity
    {
        return $this->leafAccessor;
    }

    /**
     * @param LeafEntity $leafAccessor
     * @return SecondLevelEntity
     */
    public function setLeafAccessor(LeafEntity $leafAccessor): SecondLevelEntity
    {
        $this->leafAccessor = $leafAccessor;
        return $this;
    }
}