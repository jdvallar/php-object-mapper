<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional\Models\Entity;


class BasicGetterSetterEntity
{
    /** @var string */
    private $id;

    /** @var int */
    private $attrOne;

    /** @var string */
    private $attrTwo;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BasicGetterSetterEntity
     */
    public function setId(string $id): BasicGetterSetterEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttrOne(): int
    {
        return $this->attrOne;
    }

    /**
     * @param int $attrOne
     * @return BasicGetterSetterEntity
     */
    public function setAttrOne(int $attrOne): BasicGetterSetterEntity
    {
        $this->attrOne = $attrOne;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrTwo(): string
    {
        return $this->attrTwo;
    }

    /**
     * @param string $attrTwo
     * @return BasicGetterSetterEntity
     */
    public function setAttrTwo(string $attrTwo): BasicGetterSetterEntity
    {
        $this->attrTwo = $attrTwo;
        return $this;
    }
}