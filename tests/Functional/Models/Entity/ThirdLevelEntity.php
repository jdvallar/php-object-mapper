<?php


namespace Vallarj\ObjectMapper\Tests\Functional\Models\Entity;


class ThirdLevelEntity
{
    /** @var string */
    private $id;

    /** @var string|null */
    private $attr;

    /** @var SecondLevelEntity|null */
    private $secondLevel;

    /** @var SecondLevelEntity|null */
    private $secondLevelTwo;

    /** @var SecondLevelEntity|null */
    private $secondLevelThree;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ThirdLevelEntity
     */
    public function setId(string $id): ThirdLevelEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttr(): ?string
    {
        return $this->attr;
    }

    /**
     * @param string|null $attr
     * @return ThirdLevelEntity
     */
    public function setAttr(?string $attr): ThirdLevelEntity
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return SecondLevelEntity|null
     */
    public function getSecondLevel(): ?SecondLevelEntity
    {
        return $this->secondLevel;
    }

    /**
     * @param SecondLevelEntity|null $secondLevel
     * @return ThirdLevelEntity
     */
    public function setSecondLevel(?SecondLevelEntity $secondLevel): ThirdLevelEntity
    {
        $this->secondLevel = $secondLevel;
        return $this;
    }

    /**
     * @return SecondLevelEntity|null
     */
    public function getSecondLevelTwo(): ?SecondLevelEntity
    {
        return $this->secondLevelTwo;
    }

    /**
     * @param SecondLevelEntity|null $secondLevelTwo
     * @return ThirdLevelEntity
     */
    public function setSecondLevelTwo(?SecondLevelEntity $secondLevelTwo): ThirdLevelEntity
    {
        $this->secondLevelTwo = $secondLevelTwo;
        return $this;
    }

    /**
     * @return SecondLevelEntity|null
     */
    public function getSecondLevelThree(): ?SecondLevelEntity
    {
        return $this->secondLevelThree;
    }

    /**
     * @param SecondLevelEntity|null $secondLevelThree
     * @return ThirdLevelEntity
     */
    public function setSecondLevelThree(?SecondLevelEntity $secondLevelThree): ThirdLevelEntity
    {
        $this->secondLevelThree = $secondLevelThree;
        return $this;
    }
}
