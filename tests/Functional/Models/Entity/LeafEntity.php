<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional\Models\Entity;


class LeafEntity
{
    /** @var string */
    private $id;

    /** @var int */
    private $attrOne;

    /** @var string */
    private $attrConstructor;

    public function __construct(string $attrConstructor)
    {
        $this->attrConstructor = $attrConstructor;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return LeafEntity
     */
    public function setId(string $id): LeafEntity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttrOne(): int
    {
        return $this->attrOne;
    }

    /**
     * @param int $attrOne
     * @return LeafEntity
     */
    public function setAttrOne(int $attrOne): LeafEntity
    {
        $this->attrOne = $attrOne;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrConstructor(): string
    {
        return $this->attrConstructor;
    }

    /**
     * @param string $attrConstructor
     * @return LeafEntity
     */
    public function setAttrConstructor(string $attrConstructor): LeafEntity
    {
        $this->attrConstructor = $attrConstructor;
        return $this;
    }
}