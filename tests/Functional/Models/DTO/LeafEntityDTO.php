<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional\Models\DTO;


class LeafEntityDTO
{
    /** @var string|null */
    private $id;

    /** @var int|null */
    private $attrOne;

    /** @var string|null */
    private $attrConstructor;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return LeafEntityDTO
     */
    public function setId(?string $id): LeafEntityDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAttrOne(): ?int
    {
        return $this->attrOne;
    }

    /**
     * @param int|null $attrOne
     * @return LeafEntityDTO
     */
    public function setAttrOne(?int $attrOne): LeafEntityDTO
    {
        $this->attrOne = $attrOne;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttrConstructor(): ?string
    {
        return $this->attrConstructor;
    }

    /**
     * @param string|null $attrConstructor
     * @return LeafEntityDTO
     */
    public function setAttrConstructor(?string $attrConstructor): LeafEntityDTO
    {
        $this->attrConstructor = $attrConstructor;
        return $this;
    }
}