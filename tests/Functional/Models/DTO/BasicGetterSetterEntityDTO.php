<?php
declare(strict_types=1);

namespace Vallarj\ObjectMapper\Tests\Functional\Models\DTO;


class BasicGetterSetterEntityDTO
{
    /** @var string */
    private $id;

    /** @var int */
    private $attrOne;

    /** @var string */
    private $attrTwo;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return BasicGetterSetterEntityDTO
     */
    public function setId(string $id): BasicGetterSetterEntityDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getAttrOne(): int
    {
        return $this->attrOne;
    }

    /**
     * @param int $attrOne
     * @return BasicGetterSetterEntityDTO
     */
    public function setAttrOne(int $attrOne): BasicGetterSetterEntityDTO
    {
        $this->attrOne = $attrOne;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrTwo(): string
    {
        return $this->attrTwo;
    }

    /**
     * @param string $attrTwo
     * @return BasicGetterSetterEntityDTO
     */
    public function setAttrTwo(string $attrTwo): BasicGetterSetterEntityDTO
    {
        $this->attrTwo = $attrTwo;
        return $this;
    }
}