<?php


namespace Vallarj\ObjectMapper\Tests\Functional\Models\DTO;


class SecondLevelEntityDTO
{
    /** @var string|null */
    private $id;

    /** @var string|null */
    private $attr;

    /** @var LeafEntityDTO|null */
    private $leafAccessor;

    /** @var LeafEntityDTO|null */
    private $leafConstructor;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return SecondLevelEntityDTO
     */
    public function setId(?string $id): SecondLevelEntityDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttr(): ?string
    {
        return $this->attr;
    }

    /**
     * @param string|null $attr
     * @return SecondLevelEntityDTO
     */
    public function setAttr(?string $attr): SecondLevelEntityDTO
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return LeafEntityDTO|null
     */
    public function getLeafAccessor(): ?LeafEntityDTO
    {
        return $this->leafAccessor;
    }

    /**
     * @param LeafEntityDTO|null $leafAccessor
     * @return SecondLevelEntityDTO
     */
    public function setLeafAccessor(?LeafEntityDTO $leafAccessor): SecondLevelEntityDTO
    {
        $this->leafAccessor = $leafAccessor;
        return $this;
    }

    /**
     * @return LeafEntityDTO|null
     */
    public function getLeafConstructor(): ?LeafEntityDTO
    {
        return $this->leafConstructor;
    }

    /**
     * @param LeafEntityDTO|null $leafConstructor
     * @return SecondLevelEntityDTO
     */
    public function setLeafConstructor(?LeafEntityDTO $leafConstructor): SecondLevelEntityDTO
    {
        $this->leafConstructor = $leafConstructor;
        return $this;
    }
}