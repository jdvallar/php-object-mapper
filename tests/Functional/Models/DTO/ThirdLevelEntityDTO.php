<?php


namespace Vallarj\ObjectMapper\Tests\Functional\Models\DTO;


class ThirdLevelEntityDTO
{
    /** @var string|null */
    private $id;

    /** @var string|null */
    private $attr;

    /** @var SecondLevelEntityDTO|null */
    private $secondLevel;

    /** @var SecondLevelEntityDTO|null */
    private $secondLevelTwo;

    /** @var SecondLevelEntityDTO|null */
    private $secondLevelThree;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return ThirdLevelEntityDTO
     */
    public function setId(?string $id): ThirdLevelEntityDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAttr(): ?string
    {
        return $this->attr;
    }

    /**
     * @param string|null $attr
     * @return ThirdLevelEntityDTO
     */
    public function setAttr(?string $attr): ThirdLevelEntityDTO
    {
        $this->attr = $attr;
        return $this;
    }

    /**
     * @return SecondLevelEntityDTO|null
     */
    public function getSecondLevel(): ?SecondLevelEntityDTO
    {
        return $this->secondLevel;
    }

    /**
     * @param SecondLevelEntityDTO|null $secondLevel
     * @return ThirdLevelEntityDTO
     */
    public function setSecondLevel(?SecondLevelEntityDTO $secondLevel): ThirdLevelEntityDTO
    {
        $this->secondLevel = $secondLevel;
        return $this;
    }

    /**
     * @return SecondLevelEntityDTO|null
     */
    public function getSecondLevelTwo(): ?SecondLevelEntityDTO
    {
        return $this->secondLevelTwo;
    }

    /**
     * @param SecondLevelEntityDTO|null $secondLevelTwo
     * @return ThirdLevelEntityDTO
     */
    public function setSecondLevelTwo(?SecondLevelEntityDTO $secondLevelTwo): ThirdLevelEntityDTO
    {
        $this->secondLevelTwo = $secondLevelTwo;
        return $this;
    }

    /**
     * @return SecondLevelEntityDTO|null
     */
    public function getSecondLevelThree(): ?SecondLevelEntityDTO
    {
        return $this->secondLevelThree;
    }

    /**
     * @param SecondLevelEntityDTO|null $secondLevelThree
     * @return ThirdLevelEntityDTO
     */
    public function setSecondLevelThree(?SecondLevelEntityDTO $secondLevelThree): ThirdLevelEntityDTO
    {
        $this->secondLevelThree = $secondLevelThree;
        return $this;
    }
}
