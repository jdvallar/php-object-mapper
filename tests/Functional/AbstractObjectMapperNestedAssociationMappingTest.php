<?php


namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class AbstractObjectMapperNestedAssociationMappingTest extends AutoMapperTestCase
{
    /** @var EntityA */
    private $entityA;

    /** @var EntityB */
    private $entityB;

    /** @var EntityC */
    private $entityC;

    protected function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register(new EntityADTOForwardMapper());
        $this->_autoMapper->register(new EntityBDTOForwardMapper());
        $this->_autoMapper->register(new EntityCDTOForwardMapper());

        $this->entityA = new EntityA();
        $this->entityB = new EntityB();
        $this->entityC = new EntityC();

        $this->entityA->setAttrA("attrA")
            ->setRelB($this->entityB);

        $this->entityB->setAttrB("attrB")
            ->setRelC($this->entityC);

        $this->entityC->setAttrC("attrC");
    }

    public function testShouldMapPropertiesOfNestedAssociationsWithClassTarget(): void
    {
        /** @var EntityADTO $result */
        $result = $this->_autoMapper->map($this->entityA, EntityADTO::class);

        $dtoA = $result;
        $dtoB = $result->getRelB();
        $dtoC = $dtoB->getRelC();

        $this->assertEquals($this->entityA->getId(), $dtoA->getId());
        $this->assertEquals($this->entityA->getAttrA(), $dtoA->getAttrA());
        $this->assertEquals($this->entityB->getId(), $dtoB->getId());
        $this->assertEquals($this->entityB->getAttrB(), $dtoB->getAttrB());
        $this->assertEquals($this->entityC->getId(), $dtoC->getId());
        $this->assertEquals($this->entityC->getAttrC(), $dtoC->getAttrC());
    }

    public function testShouldMapPropertiesOfNestedAssociationsWithObjectTarget(): void
    {
        /** @var EntityADTO $result */
        $result = $this->_autoMapper->map($this->entityA, new EntityADTO());

        $dtoA = $result;
        $dtoB = $result->getRelB();
        $dtoC = $dtoB->getRelC();

        $this->assertEquals($this->entityA->getId(), $dtoA->getId());
        $this->assertEquals($this->entityA->getAttrA(), $dtoA->getAttrA());
        $this->assertEquals($this->entityB->getId(), $dtoB->getId());
        $this->assertEquals($this->entityB->getAttrB(), $dtoB->getAttrB());
        $this->assertEquals($this->entityC->getId(), $dtoC->getId());
        $this->assertEquals($this->entityC->getAttrC(), $dtoC->getAttrC());
    }
}

class EntityA
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrA;

    /** @var null|EntityB */
    private $relB;

    function __construct()
    {
        $this->id = "ID-A";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityA
     */
    public function setId(string $id): EntityA
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrA(): string
    {
        return $this->attrA;
    }

    /**
     * @param string $attrA
     * @return EntityA
     */
    public function setAttrA(string $attrA): EntityA
    {
        $this->attrA = $attrA;
        return $this;
    }

    /**
     * @return EntityB|null
     */
    public function getRelB(): EntityB
    {
        return $this->relB;
    }

    /**
     * @param EntityB|null $relB
     * @return EntityA
     */
    public function setRelB(EntityB $relB): EntityA
    {
        $this->relB = $relB;
        return $this;
    }
}

class EntityB
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrB;

    /** @var EntityC */
    private $relC;

    function __construct()
    {
        $this->id = "ID-B";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityB
     */
    public function setId(string $id): EntityB
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrB(): string
    {
        return $this->attrB;
    }

    /**
     * @param string $attrB
     * @return EntityB
     */
    public function setAttrB(string $attrB): EntityB
    {
        $this->attrB = $attrB;
        return $this;
    }

    /**
     * @return EntityC
     */
    public function getRelC(): EntityC
    {
        return $this->relC;
    }

    /**
     * @param EntityC $relC
     * @return EntityB
     */
    public function setRelC(EntityC $relC): EntityB
    {
        $this->relC = $relC;
        return $this;
    }
}

class EntityC
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrC;

    function __construct()
    {
        $this->id = "ID-C";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityC
     */
    public function setId(string $id): EntityC
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrC(): string
    {
        return $this->attrC;
    }

    /**
     * @param string $attrC
     * @return EntityC
     */
    public function setAttrC(string $attrC): EntityC
    {
        $this->attrC = $attrC;
        return $this;
    }
}

class EntityADTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrA;

    /** @var EntityBDTO */
    private $relB;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityADTO
     */
    public function setId(string $id): EntityADTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrA(): string
    {
        return $this->attrA;
    }

    /**
     * @param string $attrA
     * @return EntityADTO
     */
    public function setAttrA(string $attrA): EntityADTO
    {
        $this->attrA = $attrA;
        return $this;
    }

    /**
     * @return EntityBDTO
     */
    public function getRelB(): EntityBDTO
    {
        return $this->relB;
    }

    /**
     * @param EntityBDTO $relB
     * @return EntityADTO
     */
    public function setRelB(EntityBDTO $relB): EntityADTO
    {
        $this->relB = $relB;
        return $this;
    }
}

class EntityBDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrB;

    /** @var EntityCDTO */
    private $relC;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityBDTO
     */
    public function setId(string $id): EntityBDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrB(): string
    {
        return $this->attrB;
    }

    /**
     * @param string $attrB
     * @return EntityBDTO
     */
    public function setAttrB(string $attrB): EntityBDTO
    {
        $this->attrB = $attrB;
        return $this;
    }

    /**
     * @return EntityCDTO
     */
    public function getRelC(): EntityCDTO
    {
        return $this->relC;
    }

    /**
     * @param EntityCDTO $relC
     * @return EntityBDTO
     */
    public function setRelC(EntityCDTO $relC): EntityBDTO
    {
        $this->relC = $relC;
        return $this;
    }
}

class EntityCDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrC;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return EntityCDTO
     */
    public function setId(string $id): EntityCDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrC(): string
    {
        return $this->attrC;
    }

    /**
     * @param string $attrC
     * @return EntityCDTO
     */
    public function setAttrC(string $attrC): EntityCDTO
    {
        $this->attrC = $attrC;
        return $this;
    }
}

class EntityADTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = EntityA::class;
    protected $targetClass = EntityADTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrA')
            ->mapToOneAssociation('relB', EntityBDTO::class);
    }
}

class EntityBDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = EntityB::class;
    protected $targetClass = EntityBDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrB')
            ->mapToOneAssociation('relC', EntityCDTO::class);
    }
}

class EntityCDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = EntityC::class;
    protected $targetClass = EntityCDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrC');
    }
}