<?php


namespace Vallarj\ObjectMapper\Tests\Functional;


use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class AbstractObjectMapperCyclicAssociationMappingTest extends AutoMapperTestCase
{
    /** @var CyclicA */
    private $cyclicA;

    /** @var CyclicB */
    private $cyclicB;

    protected function setUp(): void
    {
        parent::setUp();

        $this->_autoMapper->register(new CyclicADTOForwardMapper());
        $this->_autoMapper->register(new CyclicBDTOForwardMapper());

        $this->cyclicA = new CyclicA();
        $this->cyclicB = new CyclicB();

        $this->cyclicA->setAttrA("attrA")
            ->setRelB($this->cyclicB);

        $this->cyclicB->setAttrB("attrB")
            ->setRelA($this->cyclicA);
    }

    public function testShouldMapCyclicAssociationsWithClassTarget()
    {
        /** @var CyclicADTO $result */
        $result = $this->_autoMapper->map($this->cyclicA, CyclicADTO::class);

        $this->assertInstanceOf(CyclicADTO::class, $result);
        $this->assertEquals($this->cyclicA->getId(), $result->getId());
        $this->assertEquals($this->cyclicA->getAttrA(), $result->getAttrA());
        $this->assertEquals($this->cyclicB->getId(), $result->getRelB()->getId());
        $this->assertEquals($this->cyclicB->getAttrB(), $result->getRelB()->getAttrB());
        $this->assertNotNull($result->getRelB()->getRelA());
    }

    public function testShouldMapCyclicAssociationsWithObjectTarget()
    {
        /** @var CyclicADTO $result */
        $result = $this->_autoMapper->map($this->cyclicA, new CyclicADTO());

        $this->assertInstanceOf(CyclicADTO::class, $result);
        $this->assertEquals($this->cyclicA->getId(), $result->getId());
        $this->assertEquals($this->cyclicA->getAttrA(), $result->getAttrA());
        $this->assertEquals($this->cyclicB->getId(), $result->getRelB()->getId());
        $this->assertEquals($this->cyclicB->getAttrB(), $result->getRelB()->getAttrB());
        $this->assertNotNull($result->getRelB()->getRelA());
    }

    public function testShouldReuseObjectsWithSameClassAndIdentifier()
    {
        /** @var CyclicADTO $result */
        $result = $this->_autoMapper->map($this->cyclicA, CyclicADTO::class);

        $this->assertSame($result, $result->getRelB()->getRelA());
    }

    public function testShouldReuseProvidedTargetObjectInResolvingCyclicAssociations()
    {
        $target = new CyclicADTO();

        /** @var CyclicADTO $result */
        $result = $this->_autoMapper->map($this->cyclicA, $target);

        $this->assertSame($target, $result);
        $this->assertSame($target, $result->getRelB()->getRelA());
    }
}

class CyclicA
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrA;

    /** @var null|CyclicB */
    private $relB;

    function __construct()
    {
        $this->id = "ID-A";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CyclicA
     */
    public function setId(string $id): CyclicA
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrA(): string
    {
        return $this->attrA;
    }

    /**
     * @param string $attrA
     * @return CyclicA
     */
    public function setAttrA(string $attrA): CyclicA
    {
        $this->attrA = $attrA;
        return $this;
    }

    /**
     * @return CyclicB|null
     */
    public function getRelB(): CyclicB
    {
        return $this->relB;
    }

    /**
     * @param CyclicB|null $relB
     * @return CyclicA
     */
    public function setRelB(CyclicB $relB): CyclicA
    {
        $this->relB = $relB;
        return $this;
    }
}

class CyclicB
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrB;

    /** @var CyclicA */
    private $relA;

    function __construct()
    {
        $this->id = "ID-B";
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CyclicB
     */
    public function setId(string $id): CyclicB
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrB(): string
    {
        return $this->attrB;
    }

    /**
     * @param string $attrB
     * @return CyclicB
     */
    public function setAttrB(string $attrB): CyclicB
    {
        $this->attrB = $attrB;
        return $this;
    }

    /**
     * @return CyclicA
     */
    public function getRelA(): CyclicA
    {
        return $this->relA;
    }

    /**
     * @param CyclicA $relA
     * @return CyclicB
     */
    public function setRelA(CyclicA $relA): CyclicB
    {
        $this->relA = $relA;
        return $this;
    }
}

class CyclicADTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrA;

    /** @var CyclicBDTO */
    private $relB;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CyclicADTO
     */
    public function setId(string $id): CyclicADTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrA(): string
    {
        return $this->attrA;
    }

    /**
     * @param string $attrA
     * @return CyclicADTO
     */
    public function setAttrA(string $attrA): CyclicADTO
    {
        $this->attrA = $attrA;
        return $this;
    }

    /**
     * @return CyclicBDTO
     */
    public function getRelB(): CyclicBDTO
    {
        return $this->relB;
    }

    /**
     * @param CyclicBDTO $relB
     * @return CyclicADTO
     */
    public function setRelB(CyclicBDTO $relB): CyclicADTO
    {
        $this->relB = $relB;
        return $this;
    }
}

class CyclicBDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $attrB;

    /** @var CyclicADTO */
    private $relA;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return CyclicBDTO
     */
    public function setId(string $id): CyclicBDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAttrB(): string
    {
        return $this->attrB;
    }

    /**
     * @param string $attrB
     * @return CyclicBDTO
     */
    public function setAttrB(string $attrB): CyclicBDTO
    {
        $this->attrB = $attrB;
        return $this;
    }

    /**
     * @return CyclicADTO
     */
    public function getRelA(): CyclicADTO
    {
        return $this->relA;
    }

    /**
     * @param CyclicADTO $relA
     * @return CyclicBDTO
     */
    public function setRelA(CyclicADTO $relA): CyclicBDTO
    {
        $this->relA = $relA;
        return $this;
    }
}

class CyclicADTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = CyclicA::class;
    protected $targetClass = CyclicADTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrA')
            ->mapToOneAssociation('relB', CyclicBDTO::class);
    }
}

class CyclicBDTOForwardMapper extends AbstractObjectMapper
{
    protected $sourceClass = CyclicB::class;
    protected $targetClass = CyclicBDTO::class;

    public function __construct()
    {
        $this->mapAttribute('attrB')
            ->mapToOneAssociation('relA', CyclicADTO::class);
    }
}